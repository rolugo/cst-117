﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CST_117_Exercise_10
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int charFound = 0;
                StreamReader inputFile = File.OpenText("Exercise 10 file.txt");
                while (!inputFile.EndOfStream)
                {
                    string lineInfo = inputFile.ReadLine();
                    int cnt = 0;
                    foreach (char letter in lineInfo)
                    {
                        if (letter.ToString() == " " || letter.ToString() == "." || letter.ToString() == "!" ||
                            letter.ToString() == "," || letter.ToString() == "?" || letter.ToString() == ";" ||
                            letter.ToString() == ":")
                        {
                            if ((cnt != 0 && lineInfo[cnt - 1].ToString().ToUpper() == "T") ||
                                (cnt != 0 && lineInfo[cnt - 1].ToString().ToUpper() == "E"))
                            {
                                charFound += 1;
                            }
                        }
                        cnt += 1;
                    }
                }
                inputFile.Close();
                Console.WriteLine("There are " + charFound.ToString() + " words that end in t or e.");
                Console.WriteLine("Press enter to close...");
                Console.Read();
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
                Console.Read();
            }
        }
    }
}
