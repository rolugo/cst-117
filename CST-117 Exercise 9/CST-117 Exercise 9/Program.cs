﻿//Corrected by R. Lugo
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Exercise_9
{

     static class Program
    {

         [STAThread]
         static void Main(string[] args)
        {
            //make some sets
            Set A = new Set();
            Set B = new Set();

            //put some stuff in the sets
            Random r = new Random();
            for (int i = 0; i < 10; i++)
            {
                //Changed it to 10 to get a max of ten elements.
                A.AddElement(r.Next(10));
                B.AddElement(r.Next(12));
            }

            //display each set and the Union
            Console.WriteLine("A: " + A);
            Console.WriteLine("B: " + B);
            Console.WriteLine("A Union B: " + A.Union(B));

            //display original sets (should be unchanged)
            Console.WriteLine("After Union operation");
            Console.WriteLine("A: " + A);
            Console.WriteLine("B: " + B);
            //Added a Console.Readline
            Console.ReadLine();

        }
    }
}
