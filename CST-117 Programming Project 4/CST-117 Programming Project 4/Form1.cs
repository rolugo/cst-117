﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Programming_Project_4
{
    public partial class TicTacToe : Form
    {
        public TicTacToe()
        {
            InitializeComponent();
        }

        private void TTTnewGameButton_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            const int ROW = 3;
            const int COL = 3;
            int[,] gameBoard = new int[ROW, COL];

            for (int row = 0; row < ROW; row++)
            {
                for (int col = 0; col < COL; col++)
                {
                    gameBoard[row, col] = rand.Next(2);
                }
            }
            TTTLabel1.Text = GetValue(gameBoard[0, 0]);
            TTTLabel2.Text = GetValue(gameBoard[0, 1]);
            TTTLabel3.Text = GetValue(gameBoard[0, 2]);
            TTTLabel4.Text = GetValue(gameBoard[1, 0]);
            TTTLabel5.Text = GetValue(gameBoard[1, 1]);
            TTTLabel6.Text = GetValue(gameBoard[1, 2]);
            TTTLabel7.Text = GetValue(gameBoard[2, 0]);
            TTTLabel8.Text = GetValue(gameBoard[2, 1]);
            TTTLabel9.Text = GetValue(gameBoard[2, 2]);
            TTTresultsTextbox.Text = GetWinner(gameBoard);
        }
        public string GetWinner(int[,] intArray)
        {
            int winner = -1;
            if ((intArray[0, 0] == intArray[0, 1]) && (intArray[0, 0] == intArray[0, 2]))
            {
                winner = intArray[0, 0];
            }
            if ((intArray[1, 0] == intArray[1, 1]) && (intArray[1, 0] == intArray[1, 2]))
            {
                winner = intArray[1, 0];
            }
            if ((intArray[2, 0] == intArray[2, 1]) && (intArray[2, 0] == intArray[2, 2]))
            {
                winner = intArray[2, 0];
            }
            if ((intArray[0, 0] == intArray[1, 0]) && (intArray[0, 0] == intArray[2, 0]))
            {
                winner = intArray[0, 0];
            }
            if ((intArray[0, 1] == intArray[1, 1]) && (intArray[0, 1] == intArray[2, 1]))
            {
                winner = intArray[0, 1];
            }
            if ((intArray[0, 2] == intArray[1, 2]) && (intArray[0, 2] == intArray[2, 2]))
            {
                winner = intArray[0, 2];
            }
            if ((intArray[0, 0] == intArray[1, 1]) && (intArray[0, 0] == intArray[2, 2]))
            {
                winner = intArray[0, 0];
            }
            if ((intArray[0, 2] == intArray[1, 1]) && (intArray[0, 2] == intArray[2, 0]))
            {
                winner = intArray[0, 2];
            }
            if (winner == 0)
            {
                return "Player O Wins!!!";
            }
            else if (winner == 1)
            {
                return "Player X Wins!!!";
            }
            else
            {
                return "The game was a tie!!!";
            }
        }
        public string GetValue(int integerValue)
        {
            if (integerValue == 0)
            {
                return "0";
            }
            else
            {
                return "X";
            }
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
