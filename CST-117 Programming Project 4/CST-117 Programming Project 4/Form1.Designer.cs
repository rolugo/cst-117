﻿namespace CST_117_Programming_Project_4
{
    partial class TicTacToe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            this.TTTLabel1 = new System.Windows.Forms.Label();
            this.TTTLabel2 = new System.Windows.Forms.Label();
            this.TTTLabel3 = new System.Windows.Forms.Label();
            this.TTTLabel4 = new System.Windows.Forms.Label();
            this.TTTLabel5 = new System.Windows.Forms.Label();
            this.TTTLabel6 = new System.Windows.Forms.Label();
            this.TTTLabel7 = new System.Windows.Forms.Label();
            this.TTTLabel8 = new System.Windows.Forms.Label();
            this.TTTLabel9 = new System.Windows.Forms.Label();
            this.TTTresultsTextbox = new System.Windows.Forms.RichTextBox();
            this.TTTnewGameButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TTTLabel1
            // 
            this.TTTLabel1.AutoSize = true;
            this.TTTLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel1.Location = new System.Drawing.Point(9, 9);
            this.TTTLabel1.Name = "TTTLabel1";
            this.TTTLabel1.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel1.TabIndex = 0;
            this.TTTLabel1.Text = "1";
            // 
            // TTTLabel2
            // 
            this.TTTLabel2.AutoSize = true;
            this.TTTLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel2.Location = new System.Drawing.Point(91, 9);
            this.TTTLabel2.Name = "TTTLabel2";
            this.TTTLabel2.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel2.TabIndex = 1;
            this.TTTLabel2.Text = "1";
            // 
            // TTTLabel3
            // 
            this.TTTLabel3.AutoSize = true;
            this.TTTLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel3.Location = new System.Drawing.Point(173, 9);
            this.TTTLabel3.Name = "TTTLabel3";
            this.TTTLabel3.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel3.TabIndex = 2;
            this.TTTLabel3.Text = "1";
            // 
            // TTTLabel4
            // 
            this.TTTLabel4.AutoSize = true;
            this.TTTLabel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel4.Location = new System.Drawing.Point(9, 108);
            this.TTTLabel4.Name = "TTTLabel4";
            this.TTTLabel4.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel4.TabIndex = 3;
            this.TTTLabel4.Text = "1";
            // 
            // TTTLabel5
            // 
            this.TTTLabel5.AutoSize = true;
            this.TTTLabel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel5.Location = new System.Drawing.Point(91, 108);
            this.TTTLabel5.Name = "TTTLabel5";
            this.TTTLabel5.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel5.TabIndex = 4;
            this.TTTLabel5.Text = "1";
            // 
            // TTTLabel6
            // 
            this.TTTLabel6.AutoSize = true;
            this.TTTLabel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel6.Location = new System.Drawing.Point(173, 108);
            this.TTTLabel6.Name = "TTTLabel6";
            this.TTTLabel6.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel6.TabIndex = 5;
            this.TTTLabel6.Text = "1";
            // 
            // TTTLabel7
            // 
            this.TTTLabel7.AutoSize = true;
            this.TTTLabel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel7.Location = new System.Drawing.Point(9, 206);
            this.TTTLabel7.Name = "TTTLabel7";
            this.TTTLabel7.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel7.TabIndex = 6;
            this.TTTLabel7.Text = "1";
            // 
            // TTTLabel8
            // 
            this.TTTLabel8.AutoSize = true;
            this.TTTLabel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel8.Location = new System.Drawing.Point(91, 206);
            this.TTTLabel8.Name = "TTTLabel8";
            this.TTTLabel8.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel8.TabIndex = 7;
            this.TTTLabel8.Text = "1";
            // 
            // TTTLabel9
            // 
            this.TTTLabel9.AutoSize = true;
            this.TTTLabel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TTTLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTLabel9.Location = new System.Drawing.Point(173, 206);
            this.TTTLabel9.Name = "TTTLabel9";
            this.TTTLabel9.Size = new System.Drawing.Size(71, 75);
            this.TTTLabel9.TabIndex = 8;
            this.TTTLabel9.Text = "1";
            // 
            // TTTresultsTextbox
            // 
            this.TTTresultsTextbox.BackColor = System.Drawing.SystemColors.Control;
            this.TTTresultsTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TTTresultsTextbox.Location = new System.Drawing.Point(12, 311);
            this.TTTresultsTextbox.Name = "TTTresultsTextbox";
            this.TTTresultsTextbox.Size = new System.Drawing.Size(236, 28);
            this.TTTresultsTextbox.TabIndex = 10;
            this.TTTresultsTextbox.Text = "";
            // 
            // TTTnewGameButton
            // 
            this.TTTnewGameButton.Location = new System.Drawing.Point(13, 356);
            this.TTTnewGameButton.Name = "TTTnewGameButton";
            this.TTTnewGameButton.Size = new System.Drawing.Size(118, 27);
            this.TTTnewGameButton.TabIndex = 11;
            this.TTTnewGameButton.Text = "New Game";
            this.TTTnewGameButton.UseVisualStyleBackColor = true;
            this.TTTnewGameButton.Click += new System.EventHandler(this.TTTnewGameButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(137, 356);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(112, 27);
            this.ExitButton.TabIndex = 12;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // TicTacToe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 395);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.TTTnewGameButton);
            this.Controls.Add(this.TTTresultsTextbox);
            this.Controls.Add(this.TTTLabel9);
            this.Controls.Add(this.TTTLabel8);
            this.Controls.Add(this.TTTLabel7);
            this.Controls.Add(this.TTTLabel6);
            this.Controls.Add(this.TTTLabel5);
            this.Controls.Add(this.TTTLabel4);
            this.Controls.Add(this.TTTLabel3);
            this.Controls.Add(this.TTTLabel2);
            this.Controls.Add(this.TTTLabel1);
            this.Name = "TicTacToe";
            this.Text = "Tic-Tac-Toe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TTTLabel1;
        private System.Windows.Forms.Label TTTLabel2;
        private System.Windows.Forms.Label TTTLabel3;
        private System.Windows.Forms.Label TTTLabel4;
        private System.Windows.Forms.Label TTTLabel5;
        private System.Windows.Forms.Label TTTLabel6;
        private System.Windows.Forms.Label TTTLabel7;
        private System.Windows.Forms.Label TTTLabel8;
        private System.Windows.Forms.Label TTTLabel9;
        private System.Windows.Forms.RichTextBox TTTresultsTextbox;
        private System.Windows.Forms.Button TTTnewGameButton;
        private System.Windows.Forms.Button ExitButton;
    }
}
