﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Exercise_7_V2
{
    public partial class Exercise7 : Form
    {
        int sum;
        int randomSum;
        int randomNum1, randomNum2;

        public Exercise7()
        {
            InitializeComponent();
        }

        private void Cal2intBtn1_Click(object sender, EventArgs e)
        {
            if ((int.TryParse(SumInput1.Text, out int input1)) &&
                (int.TryParse(SumInput2.Text, out int input2)))
            {
                RandomSumTwoInts(input1, input2);
                SumResultLabel.Text = "The sum of " + input1 + " and " + input2 + " is " + sum.ToString();
            }
            else
            {
                MessageBox.Show("Enter an Integer value.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void AvgFiveBtn_Click(object sender, EventArgs e)
        {
            if (
                (double.TryParse(Dbl1InputTextBox.Text, out double num1)) &&
                (double.TryParse(Dbl2InputTextBox.Text, out double num2)) &&
                (double.TryParse(Dbl3InputTextBox.Text, out double num3)) &&
                (double.TryParse(Dbl4InputTextBox.Text, out double num4)) &&
                (double.TryParse(Dbl5InputTextBox.Text, out double num5)))
            {
                double results = AverageOfFiveDoubles(num1, num2, num3, num4, num5);
                DblAvgResultLabel.Text = "The average of " + num1 + " " + num2 + " " + num3 + " " + num4 + " " + num5 + " is " + results.ToString("n2");
            }
            else
            {
                MessageBox.Show("Enter an Integer value.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void CalculateRndBtn_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            randomNum1 = rnd.Next(100);
            randomNum2 = rnd.Next(100);
            RandomSumTwoInts(randomNum1, randomNum2);
            RndSumResultsLabel.Text = "The sum of " + randomNum1 + " and " + randomNum2 + " is " + randomSum.ToString();
        }
        private void CalDivideBy3Btn_Click(object sender, EventArgs e)
        {
            if (
                (int.TryParse(DivideTextBox1.Text, out int input1)) &&
                (int.TryParse(DivideTextBox2.Text, out int input2)) &&
                (int.TryParse(DivideTextBox3.Text, out int input3)))
            {
                bool results = IsDivisibleBy3(input1, input2, input3);
                if (results == true)
                {
                    DivideByThreeResultsLabel.Text = "True";
                }
                else
                {
                    DivideByThreeResultsLabel.Text = "False";
                }
            }
            else
            {
                MessageBox.Show("Enter an Integer value.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void CalFewerCharBtn_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(StringInput1TextBox.Text) || String.IsNullOrEmpty(StringInput2TextBox.Text))
            {
                MessageBox.Show("Enter a string.", "Error", MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            else
            {
                string results = IsSmallerString(StringInput1TextBox.Text, StringInput2TextBox.Text);
                FewerCharResultLabel.Text = results;
            }

        }

        private void CalLargestDlbBtn1_Click(object sender, EventArgs e)
        {
            if ((double.TryParse(LargeDblInput1TextBox.Text, out double input1)) && (double.TryParse(LargeDblInput2TextBox.Text, out double input2)))
            {
                double[] doubleArray = new double[2];
                doubleArray[0] = input1;
                doubleArray[1] = input2;
                double results = IsLargestInArray(doubleArray);
                LargeDblResultsLabel.Text = "The largest Double in the array is: " + results.ToString();
            }
            else
            {
                MessageBox.Show("Enter an Integer value.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void CalRandom50IntBtn_Click(object sender, EventArgs e)
        {
            string results = Random50Int();
            Random50IntsResultstBox.Text = results;
        }
        private void CalTwoBoolBtn_Click(object sender, EventArgs e)
        {
            if ((int.TryParse(BoolValueInput1TextBox.Text, out int input1)) && (int.TryParse(BoolValueInput2TextBox.Text, out int input2)))
            {
                bool results = CompareValues(input1, input2);
                if (results == true)
                {
                    BoolValueResultsLabel.Text = "True";
                }
                else
                {
                    BoolValueResultsLabel.Text = "False";
                }
            }
            else
            {
                MessageBox.Show("Enter an Integer value.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        private void CalIntAndDblBtn_Click(object sender, EventArgs e)
        {
            if ((int.TryParse(IntInputTextBox.Text, out int intInput)) && (double.TryParse(DblInputTextBox.Text, out double dblInput)))
            {
                string results = SumofIntAndDouble(intInput, dblInput);
                IntAndDblSumResultLabel.Text = "The sum of int " + intInput + " and double " + dblInput + " is " + results;
            }
            else
            {
                MessageBox.Show("Enter an Integer value.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void CalTwoDArrayBtn_Click(object sender, EventArgs e)
        {
            int[,] intArray = new int[3, 2] { { 15, 3 }, { 7, 2 }, { 102, 18 } };
            string results = TwoDArrayAverageCal(intArray);
            TwoDArrayResultsLabel.Text = "The average of the two-dimensional array is: " + results;
        }

        private void ClrBtn_Click(object sender, EventArgs e)
        {
            SumInput1.Text = "";
            SumInput2.Text = "";
            SumResultLabel.Text = "";
            Dbl1InputTextBox.Text = "";
            Dbl2InputTextBox.Text = "";
            Dbl3InputTextBox.Text = "";
            Dbl4InputTextBox.Text = "";
            Dbl5InputTextBox.Text = "";
            DblAvgResultLabel.Text = "";
            RndSumResultsLabel.Text = "";
            DivideTextBox1.Text = "";
            DivideTextBox2.Text = "";
            DivideTextBox3.Text = "";
            DivideByThreeResultsLabel.Text = "";
            StringInput1TextBox.Text = "";
            StringInput2TextBox.Text = "";
            FewerCharResultLabel.Text = "";
            LargeDblInput1TextBox.Text = "";
            LargeDblInput2TextBox.Text = "";
            LargeDblResultsLabel.Text = "";
            Random50IntsResultstBox.Text = "";
            BoolValueInput1TextBox.Text = "";
            BoolValueInput2TextBox.Text = "";
            BoolValueResultsLabel.Text = "";
            IntInputTextBox.Text = "";
            DblInputTextBox.Text = "";
            IntAndDblSumResultLabel.Text = "";
            TwoDArrayResultsLabel.Text = "";

        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void RandomSumTwoInts(int num1, int num2)
        {
            sum = num1 + num2;
            randomSum = randomNum1 + randomNum2;
        }

        public double AverageOfFiveDoubles(double num1, double num2, double num3, double num4, double num5)
        {
            double average = (num1 + num2 + num3 + num4 + num5) / 5;
            return average;
        }

        public bool IsDivisibleBy3(int num1, int num2, int num3)
        {
            if ((num1 + num2 + num3) % 3 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string IsSmallerString(string string1, string string2)
        {
            if (string1.Length < string2.Length)
            {
                return string1;
            }
            else
            {
                return string2;
            }
        }

        public double IsLargestInArray(double[] doubleArray)
        {
            double largestDouble = 0;
            foreach (double index in doubleArray)
            {
                if (index >= largestDouble)
                {
                    largestDouble = index;
                }

            }
            return largestDouble;
        }

        public string Random50Int()
        {
            Random rand = new Random();
            int[] intArray = new int[50];
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = rand.Next(100);
                int currentValue = intArray[i];
                sb.Append(currentValue + ", ");

            }
            return sb.ToString(); ;
        }

        public bool CompareValues(int value1, int value2)
        {
            int input1 = value1;
            int input2 = value2;
            if (input1 == input2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string SumofIntAndDouble(int intInput, double dblInput)
        {
            string sum = (intInput + dblInput).ToString("n2");
            return sum;
        }

        public string TwoDArrayAverageCal(int[,] intArray)
        {
            double totalAverage = 0;
            double total = 0;
            for (int i = 0; i < intArray.GetLength(0); i++)
            {
                for (int j = 0; j < intArray.GetLength(1); j++)
                {
                    total += intArray[i, j];
                    totalAverage = total / 6;
                }
            }
            return totalAverage.ToString("n2");
        }
    }
}

