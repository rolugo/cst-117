﻿namespace CST_117_Exercise_7_V2
{
    partial class Exercise7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cal2intBtn1 = new System.Windows.Forms.Button();
            this.SumInput1 = new System.Windows.Forms.TextBox();
            this.SumInput2 = new System.Windows.Forms.TextBox();
            this.SumInt1label = new System.Windows.Forms.Label();
            this.SumInt2Label = new System.Windows.Forms.Label();
            this.SumGroupBox = new System.Windows.Forms.GroupBox();
            this.SumResultLabel = new System.Windows.Forms.Label();
            this.Dbl1InputTextBox = new System.Windows.Forms.TextBox();
            this.Dbl2InputTextBox = new System.Windows.Forms.TextBox();
            this.Dbl3InputTextBox = new System.Windows.Forms.TextBox();
            this.Dbl4InputTextBox = new System.Windows.Forms.TextBox();
            this.Dbl5InputTextBox = new System.Windows.Forms.TextBox();
            this.AvgFiveBtn = new System.Windows.Forms.Button();
            this.Dbl1InputLabel = new System.Windows.Forms.Label();
            this.Dbl2InputLabel = new System.Windows.Forms.Label();
            this.Dbl3InputLabel = new System.Windows.Forms.Label();
            this.Dbl4InputLabel = new System.Windows.Forms.Label();
            this.Dbl5InputLabel = new System.Windows.Forms.Label();
            this.DblAvgResultLabel = new System.Windows.Forms.Label();
            this.AvgGroupBox = new System.Windows.Forms.GroupBox();
            this.ClrBtn = new System.Windows.Forms.Button();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.SumTwoRndGroupBox = new System.Windows.Forms.GroupBox();
            this.RndSumResultsLabel = new System.Windows.Forms.Label();
            this.CalculateRndBtn = new System.Windows.Forms.Button();
            this.DivisibleBy3GroupBox = new System.Windows.Forms.GroupBox();
            this.DivideTextBox2 = new System.Windows.Forms.TextBox();
            this.DivideByThreeResultsLabel = new System.Windows.Forms.Label();
            this.CalDivideBy3Btn = new System.Windows.Forms.Button();
            this.DivideTextBox1 = new System.Windows.Forms.TextBox();
            this.DivideTextBox3 = new System.Windows.Forms.TextBox();
            this.DivideLabel3 = new System.Windows.Forms.Label();
            this.DivideLabel2 = new System.Windows.Forms.Label();
            this.DivideLabel1 = new System.Windows.Forms.Label();
            this.FewerCharGroupBox = new System.Windows.Forms.GroupBox();
            this.StringInput2TextBox = new System.Windows.Forms.TextBox();
            this.FewerCharResultLabel = new System.Windows.Forms.Label();
            this.StringInput1TextBox = new System.Windows.Forms.TextBox();
            this.CalFewerCharBtn = new System.Windows.Forms.Button();
            this.StringLabel2 = new System.Windows.Forms.Label();
            this.StringLable1 = new System.Windows.Forms.Label();
            this.LargesDoubleGroupBox = new System.Windows.Forms.GroupBox();
            this.LargeDblInput2TextBox = new System.Windows.Forms.TextBox();
            this.LargeDblResultsLabel = new System.Windows.Forms.Label();
            this.LargeDblInput1TextBox = new System.Windows.Forms.TextBox();
            this.CalLargestDlbBtn1 = new System.Windows.Forms.Button();
            this.LargeDblInput2 = new System.Windows.Forms.Label();
            this.LargeDblInput1 = new System.Windows.Forms.Label();
            this.Random50IntsBtnGroupBox = new System.Windows.Forms.GroupBox();
            this.Random50IntsResultstBox = new System.Windows.Forms.RichTextBox();
            this.CalRandom50IntBtn = new System.Windows.Forms.Button();
            this.CompareBoolValuesGroupBox = new System.Windows.Forms.GroupBox();
            this.BoolValueInput2TextBox = new System.Windows.Forms.TextBox();
            this.BoolValueResultsLabel = new System.Windows.Forms.Label();
            this.BoolValueInput1TextBox = new System.Windows.Forms.TextBox();
            this.CalTwoBoolBtn = new System.Windows.Forms.Button();
            this.Bool2Label = new System.Windows.Forms.Label();
            this.Bool1Lable = new System.Windows.Forms.Label();
            this.IntAndDblSumGroupBox = new System.Windows.Forms.GroupBox();
            this.IntAndDblSumResultLabel = new System.Windows.Forms.Label();
            this.DblLable = new System.Windows.Forms.Label();
            this.DblInputTextBox = new System.Windows.Forms.TextBox();
            this.IntLable = new System.Windows.Forms.Label();
            this.CalIntAndDblBtn = new System.Windows.Forms.Button();
            this.IntInputTextBox = new System.Windows.Forms.TextBox();
            this.TwoDArrayGroupBox = new System.Windows.Forms.GroupBox();
            this.TwoDArrayResultsLabel = new System.Windows.Forms.Label();
            this.CalTwoDArrayBtn = new System.Windows.Forms.Button();
            this.SumGroupBox.SuspendLayout();
            this.AvgGroupBox.SuspendLayout();
            this.SumTwoRndGroupBox.SuspendLayout();
            this.DivisibleBy3GroupBox.SuspendLayout();
            this.FewerCharGroupBox.SuspendLayout();
            this.LargesDoubleGroupBox.SuspendLayout();
            this.Random50IntsBtnGroupBox.SuspendLayout();
            this.CompareBoolValuesGroupBox.SuspendLayout();
            this.IntAndDblSumGroupBox.SuspendLayout();
            this.TwoDArrayGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Cal2intBtn1
            // 
            this.Cal2intBtn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cal2intBtn1.Location = new System.Drawing.Point(39, 120);
            this.Cal2intBtn1.Name = "Cal2intBtn1";
            this.Cal2intBtn1.Size = new System.Drawing.Size(147, 32);
            this.Cal2intBtn1.TabIndex = 0;
            this.Cal2intBtn1.Text = "Calculate";
            this.Cal2intBtn1.UseVisualStyleBackColor = true;
            this.Cal2intBtn1.Click += new System.EventHandler(this.Cal2intBtn1_Click);
            // 
            // SumInput1
            // 
            this.SumInput1.Location = new System.Drawing.Point(23, 36);
            this.SumInput1.Name = "SumInput1";
            this.SumInput1.Size = new System.Drawing.Size(62, 20);
            this.SumInput1.TabIndex = 1;
            // 
            // SumInput2
            // 
            this.SumInput2.Location = new System.Drawing.Point(113, 37);
            this.SumInput2.Name = "SumInput2";
            this.SumInput2.Size = new System.Drawing.Size(62, 20);
            this.SumInput2.TabIndex = 2;
            // 
            // SumInt1label
            // 
            this.SumInt1label.AutoSize = true;
            this.SumInt1label.Location = new System.Drawing.Point(20, 20);
            this.SumInt1label.Name = "SumInt1label";
            this.SumInt1label.Size = new System.Drawing.Size(28, 13);
            this.SumInt1label.TabIndex = 3;
            this.SumInt1label.Text = "Int 1";
            // 
            // SumInt2Label
            // 
            this.SumInt2Label.AutoSize = true;
            this.SumInt2Label.Location = new System.Drawing.Point(134, 17);
            this.SumInt2Label.Name = "SumInt2Label";
            this.SumInt2Label.Size = new System.Drawing.Size(28, 13);
            this.SumInt2Label.TabIndex = 4;
            this.SumInt2Label.Text = "Int 2";
            // 
            // SumGroupBox
            // 
            this.SumGroupBox.Controls.Add(this.SumResultLabel);
            this.SumGroupBox.Controls.Add(this.SumInt2Label);
            this.SumGroupBox.Controls.Add(this.SumInput2);
            this.SumGroupBox.Controls.Add(this.SumInt1label);
            this.SumGroupBox.Controls.Add(this.Cal2intBtn1);
            this.SumGroupBox.Controls.Add(this.SumInput1);
            this.SumGroupBox.Location = new System.Drawing.Point(12, 12);
            this.SumGroupBox.Name = "SumGroupBox";
            this.SumGroupBox.Size = new System.Drawing.Size(210, 177);
            this.SumGroupBox.TabIndex = 5;
            this.SumGroupBox.TabStop = false;
            this.SumGroupBox.Text = "1. Sum of 2 integers";
            // 
            // SumResultLabel
            // 
            this.SumResultLabel.AutoSize = true;
            this.SumResultLabel.Location = new System.Drawing.Point(20, 76);
            this.SumResultLabel.Name = "SumResultLabel";
            this.SumResultLabel.Size = new System.Drawing.Size(0, 13);
            this.SumResultLabel.TabIndex = 6;
            // 
            // Dbl1InputTextBox
            // 
            this.Dbl1InputTextBox.Location = new System.Drawing.Point(44, 15);
            this.Dbl1InputTextBox.Name = "Dbl1InputTextBox";
            this.Dbl1InputTextBox.Size = new System.Drawing.Size(149, 20);
            this.Dbl1InputTextBox.TabIndex = 6;
            // 
            // Dbl2InputTextBox
            // 
            this.Dbl2InputTextBox.Location = new System.Drawing.Point(44, 36);
            this.Dbl2InputTextBox.Name = "Dbl2InputTextBox";
            this.Dbl2InputTextBox.Size = new System.Drawing.Size(149, 20);
            this.Dbl2InputTextBox.TabIndex = 7;
            // 
            // Dbl3InputTextBox
            // 
            this.Dbl3InputTextBox.Location = new System.Drawing.Point(44, 58);
            this.Dbl3InputTextBox.Name = "Dbl3InputTextBox";
            this.Dbl3InputTextBox.Size = new System.Drawing.Size(149, 20);
            this.Dbl3InputTextBox.TabIndex = 8;
            // 
            // Dbl4InputTextBox
            // 
            this.Dbl4InputTextBox.Location = new System.Drawing.Point(44, 80);
            this.Dbl4InputTextBox.Name = "Dbl4InputTextBox";
            this.Dbl4InputTextBox.Size = new System.Drawing.Size(149, 20);
            this.Dbl4InputTextBox.TabIndex = 9;
            // 
            // Dbl5InputTextBox
            // 
            this.Dbl5InputTextBox.Location = new System.Drawing.Point(44, 101);
            this.Dbl5InputTextBox.Name = "Dbl5InputTextBox";
            this.Dbl5InputTextBox.Size = new System.Drawing.Size(149, 20);
            this.Dbl5InputTextBox.TabIndex = 10;
            // 
            // AvgFiveBtn
            // 
            this.AvgFiveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AvgFiveBtn.Location = new System.Drawing.Point(29, 175);
            this.AvgFiveBtn.Name = "AvgFiveBtn";
            this.AvgFiveBtn.Size = new System.Drawing.Size(147, 32);
            this.AvgFiveBtn.TabIndex = 11;
            this.AvgFiveBtn.Text = "Avg";
            this.AvgFiveBtn.UseVisualStyleBackColor = true;
            this.AvgFiveBtn.Click += new System.EventHandler(this.AvgFiveBtn_Click);
            // 
            // Dbl1InputLabel
            // 
            this.Dbl1InputLabel.AutoSize = true;
            this.Dbl1InputLabel.Location = new System.Drawing.Point(6, 18);
            this.Dbl1InputLabel.Name = "Dbl1InputLabel";
            this.Dbl1InputLabel.Size = new System.Drawing.Size(32, 13);
            this.Dbl1InputLabel.TabIndex = 12;
            this.Dbl1InputLabel.Text = "Dbl 1";
            // 
            // Dbl2InputLabel
            // 
            this.Dbl2InputLabel.AutoSize = true;
            this.Dbl2InputLabel.Location = new System.Drawing.Point(6, 39);
            this.Dbl2InputLabel.Name = "Dbl2InputLabel";
            this.Dbl2InputLabel.Size = new System.Drawing.Size(32, 13);
            this.Dbl2InputLabel.TabIndex = 13;
            this.Dbl2InputLabel.Text = "Dbl 2";
            // 
            // Dbl3InputLabel
            // 
            this.Dbl3InputLabel.AutoSize = true;
            this.Dbl3InputLabel.Location = new System.Drawing.Point(6, 61);
            this.Dbl3InputLabel.Name = "Dbl3InputLabel";
            this.Dbl3InputLabel.Size = new System.Drawing.Size(32, 13);
            this.Dbl3InputLabel.TabIndex = 14;
            this.Dbl3InputLabel.Text = "Dbl 3";
            // 
            // Dbl4InputLabel
            // 
            this.Dbl4InputLabel.AutoSize = true;
            this.Dbl4InputLabel.Location = new System.Drawing.Point(6, 83);
            this.Dbl4InputLabel.Name = "Dbl4InputLabel";
            this.Dbl4InputLabel.Size = new System.Drawing.Size(32, 13);
            this.Dbl4InputLabel.TabIndex = 15;
            this.Dbl4InputLabel.Text = "Dbl 4";
            // 
            // Dbl5InputLabel
            // 
            this.Dbl5InputLabel.AutoSize = true;
            this.Dbl5InputLabel.Location = new System.Drawing.Point(6, 104);
            this.Dbl5InputLabel.Name = "Dbl5InputLabel";
            this.Dbl5InputLabel.Size = new System.Drawing.Size(32, 13);
            this.Dbl5InputLabel.TabIndex = 16;
            this.Dbl5InputLabel.Text = "Dbl 5";
            // 
            // DblAvgResultLabel
            // 
            this.DblAvgResultLabel.AutoSize = true;
            this.DblAvgResultLabel.Location = new System.Drawing.Point(10, 138);
            this.DblAvgResultLabel.Name = "DblAvgResultLabel";
            this.DblAvgResultLabel.Size = new System.Drawing.Size(0, 13);
            this.DblAvgResultLabel.TabIndex = 17;
            // 
            // AvgGroupBox
            // 
            this.AvgGroupBox.Controls.Add(this.Dbl2InputTextBox);
            this.AvgGroupBox.Controls.Add(this.DblAvgResultLabel);
            this.AvgGroupBox.Controls.Add(this.Dbl1InputTextBox);
            this.AvgGroupBox.Controls.Add(this.AvgFiveBtn);
            this.AvgGroupBox.Controls.Add(this.Dbl5InputLabel);
            this.AvgGroupBox.Controls.Add(this.Dbl3InputTextBox);
            this.AvgGroupBox.Controls.Add(this.Dbl4InputLabel);
            this.AvgGroupBox.Controls.Add(this.Dbl4InputTextBox);
            this.AvgGroupBox.Controls.Add(this.Dbl3InputLabel);
            this.AvgGroupBox.Controls.Add(this.Dbl5InputTextBox);
            this.AvgGroupBox.Controls.Add(this.Dbl2InputLabel);
            this.AvgGroupBox.Controls.Add(this.Dbl1InputLabel);
            this.AvgGroupBox.Location = new System.Drawing.Point(12, 202);
            this.AvgGroupBox.Name = "AvgGroupBox";
            this.AvgGroupBox.Size = new System.Drawing.Size(210, 213);
            this.AvgGroupBox.TabIndex = 7;
            this.AvgGroupBox.TabStop = false;
            this.AvgGroupBox.Text = "2. Average of 5 doubles";
            // 
            // ClrBtn
            // 
            this.ClrBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClrBtn.Location = new System.Drawing.Point(12, 424);
            this.ClrBtn.Name = "ClrBtn";
            this.ClrBtn.Size = new System.Drawing.Size(989, 23);
            this.ClrBtn.TabIndex = 18;
            this.ClrBtn.Text = "Clear All Fields";
            this.ClrBtn.UseVisualStyleBackColor = true;
            this.ClrBtn.Click += new System.EventHandler(this.ClrBtn_Click);
            // 
            // ExitBtn
            // 
            this.ExitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.Location = new System.Drawing.Point(12, 450);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(989, 23);
            this.ExitBtn.TabIndex = 19;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = true;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // SumTwoRndGroupBox
            // 
            this.SumTwoRndGroupBox.Controls.Add(this.RndSumResultsLabel);
            this.SumTwoRndGroupBox.Controls.Add(this.CalculateRndBtn);
            this.SumTwoRndGroupBox.Location = new System.Drawing.Point(251, 12);
            this.SumTwoRndGroupBox.Name = "SumTwoRndGroupBox";
            this.SumTwoRndGroupBox.Size = new System.Drawing.Size(213, 94);
            this.SumTwoRndGroupBox.TabIndex = 7;
            this.SumTwoRndGroupBox.TabStop = false;
            this.SumTwoRndGroupBox.Text = "3. Sum of 2 random integers";
            // 
            // RndSumResultsLabel
            // 
            this.RndSumResultsLabel.AutoSize = true;
            this.RndSumResultsLabel.Location = new System.Drawing.Point(36, 27);
            this.RndSumResultsLabel.Name = "RndSumResultsLabel";
            this.RndSumResultsLabel.Size = new System.Drawing.Size(0, 13);
            this.RndSumResultsLabel.TabIndex = 6;
            // 
            // CalculateRndBtn
            // 
            this.CalculateRndBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalculateRndBtn.Location = new System.Drawing.Point(33, 47);
            this.CalculateRndBtn.Name = "CalculateRndBtn";
            this.CalculateRndBtn.Size = new System.Drawing.Size(147, 32);
            this.CalculateRndBtn.TabIndex = 0;
            this.CalculateRndBtn.Text = "Calculate";
            this.CalculateRndBtn.UseVisualStyleBackColor = true;
            this.CalculateRndBtn.Click += new System.EventHandler(this.CalculateRndBtn_Click);
            // 
            // DivisibleBy3GroupBox
            // 
            this.DivisibleBy3GroupBox.Controls.Add(this.DivideTextBox2);
            this.DivisibleBy3GroupBox.Controls.Add(this.DivideByThreeResultsLabel);
            this.DivisibleBy3GroupBox.Controls.Add(this.CalDivideBy3Btn);
            this.DivisibleBy3GroupBox.Controls.Add(this.DivideTextBox1);
            this.DivisibleBy3GroupBox.Controls.Add(this.DivideTextBox3);
            this.DivisibleBy3GroupBox.Controls.Add(this.DivideLabel3);
            this.DivisibleBy3GroupBox.Controls.Add(this.DivideLabel2);
            this.DivisibleBy3GroupBox.Controls.Add(this.DivideLabel1);
            this.DivisibleBy3GroupBox.Location = new System.Drawing.Point(251, 112);
            this.DivisibleBy3GroupBox.Name = "DivisibleBy3GroupBox";
            this.DivisibleBy3GroupBox.Size = new System.Drawing.Size(213, 161);
            this.DivisibleBy3GroupBox.TabIndex = 18;
            this.DivisibleBy3GroupBox.TabStop = false;
            this.DivisibleBy3GroupBox.Text = "4. Divisible by 3 integers";
            // 
            // DivideTextBox2
            // 
            this.DivideTextBox2.Location = new System.Drawing.Point(51, 41);
            this.DivideTextBox2.Name = "DivideTextBox2";
            this.DivideTextBox2.Size = new System.Drawing.Size(142, 20);
            this.DivideTextBox2.TabIndex = 7;
            // 
            // DivideByThreeResultsLabel
            // 
            this.DivideByThreeResultsLabel.AutoSize = true;
            this.DivideByThreeResultsLabel.Location = new System.Drawing.Point(28, 96);
            this.DivideByThreeResultsLabel.Name = "DivideByThreeResultsLabel";
            this.DivideByThreeResultsLabel.Size = new System.Drawing.Size(0, 13);
            this.DivideByThreeResultsLabel.TabIndex = 17;
            // 
            // CalDivideBy3Btn
            // 
            this.CalDivideBy3Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalDivideBy3Btn.Location = new System.Drawing.Point(33, 117);
            this.CalDivideBy3Btn.Name = "CalDivideBy3Btn";
            this.CalDivideBy3Btn.Size = new System.Drawing.Size(147, 34);
            this.CalDivideBy3Btn.TabIndex = 11;
            this.CalDivideBy3Btn.Text = "Calculate";
            this.CalDivideBy3Btn.UseVisualStyleBackColor = true;
            this.CalDivideBy3Btn.Click += new System.EventHandler(this.CalDivideBy3Btn_Click);
            // 
            // DivideTextBox1
            // 
            this.DivideTextBox1.Location = new System.Drawing.Point(51, 20);
            this.DivideTextBox1.Name = "DivideTextBox1";
            this.DivideTextBox1.Size = new System.Drawing.Size(142, 20);
            this.DivideTextBox1.TabIndex = 6;
            // 
            // DivideTextBox3
            // 
            this.DivideTextBox3.Location = new System.Drawing.Point(51, 63);
            this.DivideTextBox3.Name = "DivideTextBox3";
            this.DivideTextBox3.Size = new System.Drawing.Size(142, 20);
            this.DivideTextBox3.TabIndex = 8;
            // 
            // DivideLabel3
            // 
            this.DivideLabel3.AutoSize = true;
            this.DivideLabel3.Location = new System.Drawing.Point(17, 70);
            this.DivideLabel3.Name = "DivideLabel3";
            this.DivideLabel3.Size = new System.Drawing.Size(28, 13);
            this.DivideLabel3.TabIndex = 14;
            this.DivideLabel3.Text = "Int 3";
            // 
            // DivideLabel2
            // 
            this.DivideLabel2.AutoSize = true;
            this.DivideLabel2.Location = new System.Drawing.Point(17, 48);
            this.DivideLabel2.Name = "DivideLabel2";
            this.DivideLabel2.Size = new System.Drawing.Size(28, 13);
            this.DivideLabel2.TabIndex = 13;
            this.DivideLabel2.Text = "Int 2";
            // 
            // DivideLabel1
            // 
            this.DivideLabel1.AutoSize = true;
            this.DivideLabel1.Location = new System.Drawing.Point(17, 27);
            this.DivideLabel1.Name = "DivideLabel1";
            this.DivideLabel1.Size = new System.Drawing.Size(28, 13);
            this.DivideLabel1.TabIndex = 12;
            this.DivideLabel1.Text = "Int 1";
            // 
            // FewerCharGroupBox
            // 
            this.FewerCharGroupBox.Controls.Add(this.StringInput2TextBox);
            this.FewerCharGroupBox.Controls.Add(this.FewerCharResultLabel);
            this.FewerCharGroupBox.Controls.Add(this.StringInput1TextBox);
            this.FewerCharGroupBox.Controls.Add(this.CalFewerCharBtn);
            this.FewerCharGroupBox.Controls.Add(this.StringLabel2);
            this.FewerCharGroupBox.Controls.Add(this.StringLable1);
            this.FewerCharGroupBox.Location = new System.Drawing.Point(251, 283);
            this.FewerCharGroupBox.Name = "FewerCharGroupBox";
            this.FewerCharGroupBox.Size = new System.Drawing.Size(213, 132);
            this.FewerCharGroupBox.TabIndex = 19;
            this.FewerCharGroupBox.TabStop = false;
            this.FewerCharGroupBox.Text = "5. The string with fewer characters";
            // 
            // StringInput2TextBox
            // 
            this.StringInput2TextBox.Location = new System.Drawing.Point(55, 36);
            this.StringInput2TextBox.Name = "StringInput2TextBox";
            this.StringInput2TextBox.Size = new System.Drawing.Size(138, 20);
            this.StringInput2TextBox.TabIndex = 7;
            // 
            // FewerCharResultLabel
            // 
            this.FewerCharResultLabel.AutoSize = true;
            this.FewerCharResultLabel.Location = new System.Drawing.Point(28, 74);
            this.FewerCharResultLabel.Name = "FewerCharResultLabel";
            this.FewerCharResultLabel.Size = new System.Drawing.Size(0, 13);
            this.FewerCharResultLabel.TabIndex = 17;
            // 
            // StringInput1TextBox
            // 
            this.StringInput1TextBox.Location = new System.Drawing.Point(55, 15);
            this.StringInput1TextBox.Name = "StringInput1TextBox";
            this.StringInput1TextBox.Size = new System.Drawing.Size(138, 20);
            this.StringInput1TextBox.TabIndex = 6;
            // 
            // CalFewerCharBtn
            // 
            this.CalFewerCharBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalFewerCharBtn.Location = new System.Drawing.Point(31, 94);
            this.CalFewerCharBtn.Name = "CalFewerCharBtn";
            this.CalFewerCharBtn.Size = new System.Drawing.Size(147, 32);
            this.CalFewerCharBtn.TabIndex = 11;
            this.CalFewerCharBtn.Text = "Calculate";
            this.CalFewerCharBtn.UseVisualStyleBackColor = true;
            this.CalFewerCharBtn.Click += new System.EventHandler(this.CalFewerCharBtn_Click);
            // 
            // StringLabel2
            // 
            this.StringLabel2.AutoSize = true;
            this.StringLabel2.Location = new System.Drawing.Point(10, 39);
            this.StringLabel2.Name = "StringLabel2";
            this.StringLabel2.Size = new System.Drawing.Size(43, 13);
            this.StringLabel2.TabIndex = 13;
            this.StringLabel2.Text = "String 2";
            // 
            // StringLable1
            // 
            this.StringLable1.AutoSize = true;
            this.StringLable1.Location = new System.Drawing.Point(10, 18);
            this.StringLable1.Name = "StringLable1";
            this.StringLable1.Size = new System.Drawing.Size(43, 13);
            this.StringLable1.TabIndex = 12;
            this.StringLable1.Text = "String 1";
            // 
            // LargesDoubleGroupBox
            // 
            this.LargesDoubleGroupBox.Controls.Add(this.LargeDblInput2TextBox);
            this.LargesDoubleGroupBox.Controls.Add(this.LargeDblResultsLabel);
            this.LargesDoubleGroupBox.Controls.Add(this.LargeDblInput1TextBox);
            this.LargesDoubleGroupBox.Controls.Add(this.CalLargestDlbBtn1);
            this.LargesDoubleGroupBox.Controls.Add(this.LargeDblInput2);
            this.LargesDoubleGroupBox.Controls.Add(this.LargeDblInput1);
            this.LargesDoubleGroupBox.Location = new System.Drawing.Point(501, 12);
            this.LargesDoubleGroupBox.Name = "LargesDoubleGroupBox";
            this.LargesDoubleGroupBox.Size = new System.Drawing.Size(212, 140);
            this.LargesDoubleGroupBox.TabIndex = 20;
            this.LargesDoubleGroupBox.TabStop = false;
            this.LargesDoubleGroupBox.Text = "6. Largest Double in the Array";
            // 
            // LargeDblInput2TextBox
            // 
            this.LargeDblInput2TextBox.Location = new System.Drawing.Point(137, 32);
            this.LargeDblInput2TextBox.Name = "LargeDblInput2TextBox";
            this.LargeDblInput2TextBox.Size = new System.Drawing.Size(54, 20);
            this.LargeDblInput2TextBox.TabIndex = 7;
            // 
            // LargeDblResultsLabel
            // 
            this.LargeDblResultsLabel.AutoSize = true;
            this.LargeDblResultsLabel.Location = new System.Drawing.Point(30, 67);
            this.LargeDblResultsLabel.Name = "LargeDblResultsLabel";
            this.LargeDblResultsLabel.Size = new System.Drawing.Size(0, 13);
            this.LargeDblResultsLabel.TabIndex = 17;
            // 
            // LargeDblInput1TextBox
            // 
            this.LargeDblInput1TextBox.Location = new System.Drawing.Point(42, 32);
            this.LargeDblInput1TextBox.Name = "LargeDblInput1TextBox";
            this.LargeDblInput1TextBox.Size = new System.Drawing.Size(54, 20);
            this.LargeDblInput1TextBox.TabIndex = 6;
            // 
            // CalLargestDlbBtn1
            // 
            this.CalLargestDlbBtn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalLargestDlbBtn1.Location = new System.Drawing.Point(33, 100);
            this.CalLargestDlbBtn1.Name = "CalLargestDlbBtn1";
            this.CalLargestDlbBtn1.Size = new System.Drawing.Size(147, 32);
            this.CalLargestDlbBtn1.TabIndex = 11;
            this.CalLargestDlbBtn1.Text = "Calculate";
            this.CalLargestDlbBtn1.UseVisualStyleBackColor = true;
            this.CalLargestDlbBtn1.Click += new System.EventHandler(this.CalLargestDlbBtn1_Click);
            // 
            // LargeDblInput2
            // 
            this.LargeDblInput2.AutoSize = true;
            this.LargeDblInput2.Location = new System.Drawing.Point(99, 32);
            this.LargeDblInput2.Name = "LargeDblInput2";
            this.LargeDblInput2.Size = new System.Drawing.Size(32, 13);
            this.LargeDblInput2.TabIndex = 13;
            this.LargeDblInput2.Text = "Dbl 2";
            // 
            // LargeDblInput1
            // 
            this.LargeDblInput1.AutoSize = true;
            this.LargeDblInput1.Location = new System.Drawing.Point(4, 32);
            this.LargeDblInput1.Name = "LargeDblInput1";
            this.LargeDblInput1.Size = new System.Drawing.Size(32, 13);
            this.LargeDblInput1.TabIndex = 12;
            this.LargeDblInput1.Text = "Dbl 1";
            // 
            // Random50IntsBtnGroupBox
            // 
            this.Random50IntsBtnGroupBox.Controls.Add(this.Random50IntsResultstBox);
            this.Random50IntsBtnGroupBox.Controls.Add(this.CalRandom50IntBtn);
            this.Random50IntsBtnGroupBox.Location = new System.Drawing.Point(501, 161);
            this.Random50IntsBtnGroupBox.Name = "Random50IntsBtnGroupBox";
            this.Random50IntsBtnGroupBox.Size = new System.Drawing.Size(212, 254);
            this.Random50IntsBtnGroupBox.TabIndex = 21;
            this.Random50IntsBtnGroupBox.TabStop = false;
            this.Random50IntsBtnGroupBox.Text = "7. 50 random integer";
            // 
            // Random50IntsResultstBox
            // 
            this.Random50IntsResultstBox.Location = new System.Drawing.Point(15, 24);
            this.Random50IntsResultstBox.Name = "Random50IntsResultstBox";
            this.Random50IntsResultstBox.Size = new System.Drawing.Size(179, 185);
            this.Random50IntsResultstBox.TabIndex = 12;
            this.Random50IntsResultstBox.Text = "";
            // 
            // CalRandom50IntBtn
            // 
            this.CalRandom50IntBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalRandom50IntBtn.Location = new System.Drawing.Point(33, 216);
            this.CalRandom50IntBtn.Name = "CalRandom50IntBtn";
            this.CalRandom50IntBtn.Size = new System.Drawing.Size(147, 32);
            this.CalRandom50IntBtn.TabIndex = 11;
            this.CalRandom50IntBtn.Text = "Calculate";
            this.CalRandom50IntBtn.UseVisualStyleBackColor = true;
            this.CalRandom50IntBtn.Click += new System.EventHandler(this.CalRandom50IntBtn_Click);
            // 
            // CompareBoolValuesGroupBox
            // 
            this.CompareBoolValuesGroupBox.Controls.Add(this.BoolValueInput2TextBox);
            this.CompareBoolValuesGroupBox.Controls.Add(this.BoolValueResultsLabel);
            this.CompareBoolValuesGroupBox.Controls.Add(this.BoolValueInput1TextBox);
            this.CompareBoolValuesGroupBox.Controls.Add(this.CalTwoBoolBtn);
            this.CompareBoolValuesGroupBox.Controls.Add(this.Bool2Label);
            this.CompareBoolValuesGroupBox.Controls.Add(this.Bool1Lable);
            this.CompareBoolValuesGroupBox.Location = new System.Drawing.Point(745, 12);
            this.CompareBoolValuesGroupBox.Name = "CompareBoolValuesGroupBox";
            this.CompareBoolValuesGroupBox.Size = new System.Drawing.Size(256, 152);
            this.CompareBoolValuesGroupBox.TabIndex = 20;
            this.CompareBoolValuesGroupBox.TabStop = false;
            this.CompareBoolValuesGroupBox.Text = "8. Compare 2 bool variables";
            // 
            // BoolValueInput2TextBox
            // 
            this.BoolValueInput2TextBox.Location = new System.Drawing.Point(77, 48);
            this.BoolValueInput2TextBox.Name = "BoolValueInput2TextBox";
            this.BoolValueInput2TextBox.Size = new System.Drawing.Size(138, 20);
            this.BoolValueInput2TextBox.TabIndex = 7;
            // 
            // BoolValueResultsLabel
            // 
            this.BoolValueResultsLabel.AutoSize = true;
            this.BoolValueResultsLabel.Location = new System.Drawing.Point(32, 81);
            this.BoolValueResultsLabel.Name = "BoolValueResultsLabel";
            this.BoolValueResultsLabel.Size = new System.Drawing.Size(0, 13);
            this.BoolValueResultsLabel.TabIndex = 17;
            // 
            // BoolValueInput1TextBox
            // 
            this.BoolValueInput1TextBox.Location = new System.Drawing.Point(77, 27);
            this.BoolValueInput1TextBox.Name = "BoolValueInput1TextBox";
            this.BoolValueInput1TextBox.Size = new System.Drawing.Size(138, 20);
            this.BoolValueInput1TextBox.TabIndex = 6;
            // 
            // CalTwoBoolBtn
            // 
            this.CalTwoBoolBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalTwoBoolBtn.Location = new System.Drawing.Point(57, 108);
            this.CalTwoBoolBtn.Name = "CalTwoBoolBtn";
            this.CalTwoBoolBtn.Size = new System.Drawing.Size(147, 32);
            this.CalTwoBoolBtn.TabIndex = 11;
            this.CalTwoBoolBtn.Text = "Calculate";
            this.CalTwoBoolBtn.UseVisualStyleBackColor = true;
            this.CalTwoBoolBtn.Click += new System.EventHandler(this.CalTwoBoolBtn_Click);
            // 
            // Bool2Label
            // 
            this.Bool2Label.AutoSize = true;
            this.Bool2Label.Location = new System.Drawing.Point(32, 51);
            this.Bool2Label.Name = "Bool2Label";
            this.Bool2Label.Size = new System.Drawing.Size(43, 13);
            this.Bool2Label.TabIndex = 13;
            this.Bool2Label.Text = "Value 2";
            // 
            // Bool1Lable
            // 
            this.Bool1Lable.AutoSize = true;
            this.Bool1Lable.Location = new System.Drawing.Point(32, 30);
            this.Bool1Lable.Name = "Bool1Lable";
            this.Bool1Lable.Size = new System.Drawing.Size(43, 13);
            this.Bool1Lable.TabIndex = 12;
            this.Bool1Lable.Text = "Valeu 1";
            // 
            // IntAndDblSumGroupBox
            // 
            this.IntAndDblSumGroupBox.Controls.Add(this.IntAndDblSumResultLabel);
            this.IntAndDblSumGroupBox.Controls.Add(this.DblLable);
            this.IntAndDblSumGroupBox.Controls.Add(this.DblInputTextBox);
            this.IntAndDblSumGroupBox.Controls.Add(this.IntLable);
            this.IntAndDblSumGroupBox.Controls.Add(this.CalIntAndDblBtn);
            this.IntAndDblSumGroupBox.Controls.Add(this.IntInputTextBox);
            this.IntAndDblSumGroupBox.Location = new System.Drawing.Point(745, 175);
            this.IntAndDblSumGroupBox.Name = "IntAndDblSumGroupBox";
            this.IntAndDblSumGroupBox.Size = new System.Drawing.Size(256, 127);
            this.IntAndDblSumGroupBox.TabIndex = 7;
            this.IntAndDblSumGroupBox.TabStop = false;
            this.IntAndDblSumGroupBox.Text = "9. Sum of int and double";
            // 
            // IntAndDblSumResultLabel
            // 
            this.IntAndDblSumResultLabel.AutoSize = true;
            this.IntAndDblSumResultLabel.Location = new System.Drawing.Point(32, 63);
            this.IntAndDblSumResultLabel.Name = "IntAndDblSumResultLabel";
            this.IntAndDblSumResultLabel.Size = new System.Drawing.Size(0, 13);
            this.IntAndDblSumResultLabel.TabIndex = 6;
            // 
            // DblLable
            // 
            this.DblLable.AutoSize = true;
            this.DblLable.Location = new System.Drawing.Point(141, 16);
            this.DblLable.Name = "DblLable";
            this.DblLable.Size = new System.Drawing.Size(32, 13);
            this.DblLable.TabIndex = 4;
            this.DblLable.Text = "Dbl 1";
            // 
            // DblInputTextBox
            // 
            this.DblInputTextBox.Location = new System.Drawing.Point(138, 32);
            this.DblInputTextBox.Name = "DblInputTextBox";
            this.DblInputTextBox.Size = new System.Drawing.Size(65, 20);
            this.DblInputTextBox.TabIndex = 2;
            // 
            // IntLable
            // 
            this.IntLable.AutoSize = true;
            this.IntLable.Location = new System.Drawing.Point(59, 17);
            this.IntLable.Name = "IntLable";
            this.IntLable.Size = new System.Drawing.Size(28, 13);
            this.IntLable.TabIndex = 3;
            this.IntLable.Text = "Int 1";
            // 
            // CalIntAndDblBtn
            // 
            this.CalIntAndDblBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalIntAndDblBtn.Location = new System.Drawing.Point(57, 85);
            this.CalIntAndDblBtn.Name = "CalIntAndDblBtn";
            this.CalIntAndDblBtn.Size = new System.Drawing.Size(147, 33);
            this.CalIntAndDblBtn.TabIndex = 0;
            this.CalIntAndDblBtn.Text = "Calculate";
            this.CalIntAndDblBtn.UseVisualStyleBackColor = true;
            this.CalIntAndDblBtn.Click += new System.EventHandler(this.CalIntAndDblBtn_Click);
            // 
            // IntInputTextBox
            // 
            this.IntInputTextBox.Location = new System.Drawing.Point(56, 33);
            this.IntInputTextBox.Name = "IntInputTextBox";
            this.IntInputTextBox.Size = new System.Drawing.Size(65, 20);
            this.IntInputTextBox.TabIndex = 1;
            // 
            // TwoDArrayGroupBox
            // 
            this.TwoDArrayGroupBox.Controls.Add(this.TwoDArrayResultsLabel);
            this.TwoDArrayGroupBox.Controls.Add(this.CalTwoDArrayBtn);
            this.TwoDArrayGroupBox.Location = new System.Drawing.Point(745, 307);
            this.TwoDArrayGroupBox.Name = "TwoDArrayGroupBox";
            this.TwoDArrayGroupBox.Size = new System.Drawing.Size(256, 108);
            this.TwoDArrayGroupBox.TabIndex = 8;
            this.TwoDArrayGroupBox.TabStop = false;
            this.TwoDArrayGroupBox.Text = "10. Average of a Two-dimension array";
            // 
            // TwoDArrayResultsLabel
            // 
            this.TwoDArrayResultsLabel.AutoSize = true;
            this.TwoDArrayResultsLabel.Location = new System.Drawing.Point(10, 28);
            this.TwoDArrayResultsLabel.Name = "TwoDArrayResultsLabel";
            this.TwoDArrayResultsLabel.Size = new System.Drawing.Size(0, 13);
            this.TwoDArrayResultsLabel.TabIndex = 6;
            // 
            // CalTwoDArrayBtn
            // 
            this.CalTwoDArrayBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalTwoDArrayBtn.Location = new System.Drawing.Point(57, 60);
            this.CalTwoDArrayBtn.Name = "CalTwoDArrayBtn";
            this.CalTwoDArrayBtn.Size = new System.Drawing.Size(147, 32);
            this.CalTwoDArrayBtn.TabIndex = 0;
            this.CalTwoDArrayBtn.Text = "Calculate";
            this.CalTwoDArrayBtn.UseVisualStyleBackColor = true;
            this.CalTwoDArrayBtn.Click += new System.EventHandler(this.CalTwoDArrayBtn_Click);
            // 
            // Exercise7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CST_117_Exercise_7_V2.Properties.Resources.Exercise_7_Background;
            this.ClientSize = new System.Drawing.Size(1016, 481);
            this.Controls.Add(this.TwoDArrayGroupBox);
            this.Controls.Add(this.IntAndDblSumGroupBox);
            this.Controls.Add(this.CompareBoolValuesGroupBox);
            this.Controls.Add(this.Random50IntsBtnGroupBox);
            this.Controls.Add(this.LargesDoubleGroupBox);
            this.Controls.Add(this.FewerCharGroupBox);
            this.Controls.Add(this.DivisibleBy3GroupBox);
            this.Controls.Add(this.SumTwoRndGroupBox);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.ClrBtn);
            this.Controls.Add(this.AvgGroupBox);
            this.Controls.Add(this.SumGroupBox);
            this.Name = "Exercise7";
            this.Text = "Exercise 7";
            this.SumGroupBox.ResumeLayout(false);
            this.SumGroupBox.PerformLayout();
            this.AvgGroupBox.ResumeLayout(false);
            this.AvgGroupBox.PerformLayout();
            this.SumTwoRndGroupBox.ResumeLayout(false);
            this.SumTwoRndGroupBox.PerformLayout();
            this.DivisibleBy3GroupBox.ResumeLayout(false);
            this.DivisibleBy3GroupBox.PerformLayout();
            this.FewerCharGroupBox.ResumeLayout(false);
            this.FewerCharGroupBox.PerformLayout();
            this.LargesDoubleGroupBox.ResumeLayout(false);
            this.LargesDoubleGroupBox.PerformLayout();
            this.Random50IntsBtnGroupBox.ResumeLayout(false);
            this.CompareBoolValuesGroupBox.ResumeLayout(false);
            this.CompareBoolValuesGroupBox.PerformLayout();
            this.IntAndDblSumGroupBox.ResumeLayout(false);
            this.IntAndDblSumGroupBox.PerformLayout();
            this.TwoDArrayGroupBox.ResumeLayout(false);
            this.TwoDArrayGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cal2intBtn1;
        private System.Windows.Forms.TextBox SumInput1;
        private System.Windows.Forms.TextBox SumInput2;
        private System.Windows.Forms.Label SumInt1label;
        private System.Windows.Forms.Label SumInt2Label;
        private System.Windows.Forms.GroupBox SumGroupBox;
        private System.Windows.Forms.Label SumResultLabel;
        private System.Windows.Forms.TextBox Dbl1InputTextBox;
        private System.Windows.Forms.TextBox Dbl2InputTextBox;
        private System.Windows.Forms.TextBox Dbl3InputTextBox;
        private System.Windows.Forms.TextBox Dbl4InputTextBox;
        private System.Windows.Forms.TextBox Dbl5InputTextBox;
        private System.Windows.Forms.Button AvgFiveBtn;
        private System.Windows.Forms.Label Dbl1InputLabel;
        private System.Windows.Forms.Label Dbl2InputLabel;
        private System.Windows.Forms.Label Dbl3InputLabel;
        private System.Windows.Forms.Label Dbl4InputLabel;
        private System.Windows.Forms.Label Dbl5InputLabel;
        private System.Windows.Forms.Label DblAvgResultLabel;
        private System.Windows.Forms.GroupBox AvgGroupBox;
        private System.Windows.Forms.Button ClrBtn;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.GroupBox SumTwoRndGroupBox;
        private System.Windows.Forms.Label RndSumResultsLabel;
        private System.Windows.Forms.Button CalculateRndBtn;
        private System.Windows.Forms.GroupBox DivisibleBy3GroupBox;
        private System.Windows.Forms.TextBox DivideTextBox2;
        private System.Windows.Forms.Label DivideByThreeResultsLabel;
        private System.Windows.Forms.TextBox DivideTextBox1;
        private System.Windows.Forms.Button CalDivideBy3Btn;
        private System.Windows.Forms.TextBox DivideTextBox3;
        private System.Windows.Forms.Label DivideLabel3;
        private System.Windows.Forms.Label DivideLabel2;
        private System.Windows.Forms.Label DivideLabel1;
        private System.Windows.Forms.GroupBox FewerCharGroupBox;
        private System.Windows.Forms.TextBox StringInput2TextBox;
        private System.Windows.Forms.Label FewerCharResultLabel;
        private System.Windows.Forms.TextBox StringInput1TextBox;
        private System.Windows.Forms.Button CalFewerCharBtn;
        private System.Windows.Forms.Label StringLabel2;
        private System.Windows.Forms.Label StringLable1;
        private System.Windows.Forms.GroupBox LargesDoubleGroupBox;
        private System.Windows.Forms.TextBox LargeDblInput2TextBox;
        private System.Windows.Forms.Label LargeDblResultsLabel;
        private System.Windows.Forms.TextBox LargeDblInput1TextBox;
        private System.Windows.Forms.Button CalLargestDlbBtn1;
        private System.Windows.Forms.Label LargeDblInput2;
        private System.Windows.Forms.Label LargeDblInput1;
        private System.Windows.Forms.GroupBox Random50IntsBtnGroupBox;
        private System.Windows.Forms.RichTextBox Random50IntsResultstBox;
        private System.Windows.Forms.Button CalRandom50IntBtn;
        private System.Windows.Forms.GroupBox CompareBoolValuesGroupBox;
        private System.Windows.Forms.TextBox BoolValueInput2TextBox;
        private System.Windows.Forms.Label BoolValueResultsLabel;
        private System.Windows.Forms.TextBox BoolValueInput1TextBox;
        private System.Windows.Forms.Button CalTwoBoolBtn;
        private System.Windows.Forms.Label Bool2Label;
        private System.Windows.Forms.Label Bool1Lable;
        private System.Windows.Forms.GroupBox IntAndDblSumGroupBox;
        private System.Windows.Forms.Label IntAndDblSumResultLabel;
        private System.Windows.Forms.Label DblLable;
        private System.Windows.Forms.TextBox DblInputTextBox;
        private System.Windows.Forms.Label IntLable;
        private System.Windows.Forms.Button CalIntAndDblBtn;
        private System.Windows.Forms.GroupBox TwoDArrayGroupBox;
        private System.Windows.Forms.Label TwoDArrayResultsLabel;
        private System.Windows.Forms.Button CalTwoDArrayBtn;
        private System.Windows.Forms.TextBox IntInputTextBox;
    }
}
