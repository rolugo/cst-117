﻿namespace CST_117_Programming_Project_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            this.ShapeList = new System.Windows.Forms.ListBox();
            this.SelectAShapeLabel = new System.Windows.Forms.Label();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            this.SelectFillModeGroupBox = new System.Windows.Forms.GroupBox();
            this.OutlineRdBtn = new System.Windows.Forms.RadioButton();
            this.FillRdBtn = new System.Windows.Forms.RadioButton();
            this.SelectDetailsGroupBox = new System.Windows.Forms.GroupBox();
            this.DateCheckbox = new System.Windows.Forms.CheckBox();
            this.NameCheckbox = new System.Windows.Forms.CheckBox();
            this.DrawBtn = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.NameLabel = new System.Windows.Forms.Label();
            this.DateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.SelectFillModeGroupBox.SuspendLayout();
            this.SelectDetailsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ShapeList
            // 
            this.ShapeList.FormattingEnabled = true;
            this.ShapeList.Items.AddRange(new object[] {"Circle", "Rectangle"});
            this.ShapeList.Location = new System.Drawing.Point(35, 47);
            this.ShapeList.Name = "ShapeList";
            this.ShapeList.Size = new System.Drawing.Size(120, 30);
            this.ShapeList.TabIndex = 0;
            // 
            // SelectAShapeLabel
            // 
            this.SelectAShapeLabel.AutoSize = true;
            this.SelectAShapeLabel.Location = new System.Drawing.Point(32, 31);
            this.SelectAShapeLabel.Name = "SelectAShapeLabel";
            this.SelectAShapeLabel.Size = new System.Drawing.Size(81, 13);
            this.SelectAShapeLabel.TabIndex = 1;
            this.SelectAShapeLabel.Text = "Select a shape:";
            // 
            // PictureBox
            // 
            this.PictureBox.BackColor = System.Drawing.SystemColors.Window;
            this.PictureBox.Location = new System.Drawing.Point(262, 31);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(270, 223);
            this.PictureBox.TabIndex = 2;
            this.PictureBox.TabStop = false;
            this.PictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox_Paint);
            // 
            // SelectFillModeGroupBox
            // 
            this.SelectFillModeGroupBox.Controls.Add(this.OutlineRdBtn);
            this.SelectFillModeGroupBox.Controls.Add(this.FillRdBtn);
            this.SelectFillModeGroupBox.Location = new System.Drawing.Point(35, 102);
            this.SelectFillModeGroupBox.Name = "SelectFillModeGroupBox";
            this.SelectFillModeGroupBox.Size = new System.Drawing.Size(156, 100);
            this.SelectFillModeGroupBox.TabIndex = 3;
            this.SelectFillModeGroupBox.TabStop = false;
            this.SelectFillModeGroupBox.Text = "Select fill mode";
            // 
            // OutlineRdBtn
            // 
            this.OutlineRdBtn.AutoSize = true;
            this.OutlineRdBtn.Location = new System.Drawing.Point(20, 64);
            this.OutlineRdBtn.Name = "OutlineRdBtn";
            this.OutlineRdBtn.Size = new System.Drawing.Size(58, 17);
            this.OutlineRdBtn.TabIndex = 1;
            this.OutlineRdBtn.TabStop = true;
            this.OutlineRdBtn.Text = "Outline";
            this.OutlineRdBtn.UseVisualStyleBackColor = true;
            // 
            // FillRdBtn
            // 
            this.FillRdBtn.AutoSize = true;
            this.FillRdBtn.Location = new System.Drawing.Point(20, 31);
            this.FillRdBtn.Name = "FillRdBtn";
            this.FillRdBtn.Size = new System.Drawing.Size(37, 17);
            this.FillRdBtn.TabIndex = 0;
            this.FillRdBtn.TabStop = true;
            this.FillRdBtn.Text = "Fill";
            this.FillRdBtn.UseVisualStyleBackColor = true;
            // 
            // SelectDetailsGroupBox
            // 
            this.SelectDetailsGroupBox.Controls.Add(this.DateCheckbox);
            this.SelectDetailsGroupBox.Controls.Add(this.NameCheckbox);
            this.SelectDetailsGroupBox.Location = new System.Drawing.Point(35, 227);
            this.SelectDetailsGroupBox.Name = "SelectDetailsGroupBox";
            this.SelectDetailsGroupBox.Size = new System.Drawing.Size(156, 100);
            this.SelectDetailsGroupBox.TabIndex = 4;
            this.SelectDetailsGroupBox.TabStop = false;
            this.SelectDetailsGroupBox.Text = "Select details";
            // 
            // DateCheckbox
            // 
            this.DateCheckbox.AutoSize = true;
            this.DateCheckbox.Location = new System.Drawing.Point(20, 63);
            this.DateCheckbox.Name = "DateCheckbox";
            this.DateCheckbox.Size = new System.Drawing.Size(49, 17);
            this.DateCheckbox.TabIndex = 1;
            this.DateCheckbox.Text = "Date";
            this.DateCheckbox.UseVisualStyleBackColor = true;
            // 
            // NameCheckbox
            // 
            this.NameCheckbox.AutoSize = true;
            this.NameCheckbox.Location = new System.Drawing.Point(20, 25);
            this.NameCheckbox.Name = "NameCheckbox";
            this.NameCheckbox.Size = new System.Drawing.Size(54, 17);
            this.NameCheckbox.TabIndex = 0;
            this.NameCheckbox.Text = "Name";
            this.NameCheckbox.UseVisualStyleBackColor = true;
            // 
            // DrawBtn
            // 
            this.DrawBtn.Location = new System.Drawing.Point(262, 290);
            this.DrawBtn.Name = "DrawBtn";
            this.DrawBtn.Size = new System.Drawing.Size(124, 37);
            this.DrawBtn.TabIndex = 5;
            this.DrawBtn.Text = "Draw";
            this.DrawBtn.UseVisualStyleBackColor = true;
            this.DrawBtn.Click += new System.EventHandler(this.DrawBtn_Click);
            // 
            // ClearBtn
            // 
            this.ClearBtn.Location = new System.Drawing.Point(408, 290);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(124, 37);
            this.ClearBtn.TabIndex = 6;
            this.ClearBtn.Text = "Clear";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(262, 261);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(0, 13);
            this.NameLabel.TabIndex = 7;
            // 
            // DateLabel
            // 
            this.DateLabel.AutoSize = true;
            this.DateLabel.Location = new System.Drawing.Point(408, 261);
            this.DateLabel.Name = "DateLabel";
            this.DateLabel.Size = new System.Drawing.Size(0, 13);
            this.DateLabel.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 362);
            this.Controls.Add(this.DateLabel);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.ClearBtn);
            this.Controls.Add(this.DrawBtn);
            this.Controls.Add(this.SelectDetailsGroupBox);
            this.Controls.Add(this.SelectFillModeGroupBox);
            this.Controls.Add(this.PictureBox);
            this.Controls.Add(this.SelectAShapeLabel);
            this.Controls.Add(this.ShapeList);
            this.Name = "Form1";
            this.Text = "Shape Maker";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.SelectFillModeGroupBox.ResumeLayout(false);
            this.SelectFillModeGroupBox.PerformLayout();
            this.SelectDetailsGroupBox.ResumeLayout(false);
            this.SelectDetailsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ShapeList;
        private System.Windows.Forms.Label SelectAShapeLabel;
        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.GroupBox SelectFillModeGroupBox;
        private System.Windows.Forms.RadioButton OutlineRdBtn;
        private System.Windows.Forms.RadioButton FillRdBtn;
        private System.Windows.Forms.GroupBox SelectDetailsGroupBox;
        private System.Windows.Forms.CheckBox DateCheckbox;
        private System.Windows.Forms.CheckBox NameCheckbox;
        private System.Windows.Forms.Button DrawBtn;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label DateLabel;
    }
}
