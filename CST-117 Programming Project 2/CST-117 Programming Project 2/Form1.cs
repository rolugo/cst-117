﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Programming_Project_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            PictureBox.Image = new Bitmap(PictureBox.Width, PictureBox.Height);
        }

        private void DrawBtn_Click(object sender, EventArgs e)
        {
            using (var g = Graphics.FromImage(PictureBox.Image))
            {
                SolidBrush myBrush = new SolidBrush(Color.Blue);
                Pen myPen = new Pen(Color.Blue);

                string shape;
                if (ShapeList.SelectedIndex != -1)
                {
                    shape = ShapeList.SelectedItem.ToString();

                    switch (shape)
                    {
                        case "Rectangle":
                            if (FillRdBtn.Checked)
                            {
                                g.FillRectangle(myBrush, 35, 50, 200, 100);
                                PictureBox.Refresh();
                            }
                           else
                           {
                                g.DrawRectangle(myPen, 35, 50, 200, 100);
                                PictureBox.Refresh();
                           }
                            if (NameCheckbox.Checked)
                            {
                                NameLabel.Text = "Rectangle";
                            }
                            if (DateCheckbox.Checked)
                            {
                                DateLabel.Text = "07/18/2019";
                            }
                            break;

                        case "Circle":
                            if (FillRdBtn.Checked)
                            {
                                g.FillEllipse(myBrush, 40, 10, 200, 200);
                                PictureBox.Refresh();
                            }
                           else
                           {
                                g.DrawEllipse(myPen, 40, 10, 200, 200);
                                PictureBox.Refresh();
                           }
                            if (NameCheckbox.Checked)
                            {
                                NameLabel.Text = "Circle";
                            }
                            if (DateCheckbox.Checked)
                            {
                                DateLabel.Text = "07/18/2019";
                            }
                            break;
                    }
                }
               else
               {
                    MessageBox.Show("Please select a shape");
               }

            }
        }

        private void PictureBox_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            PictureBox.Image = new Bitmap(PictureBox.Width, PictureBox.Height);
            PictureBox.Refresh();
            NameLabel.Text = "";
            DateLabel.Text = "";
        }
    }
}
