﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Exercise_8
{
    public partial class NutritionistCal : Form
    {
        public NutritionistCal()
        {
            InitializeComponent();
        }

        private void FatConvBtn_Click(object sender, EventArgs e)
        {
            if (double.TryParse(FatGramsTextBox.Text, out double fatGrams))
            {
                double results = FatCalories(fatGrams);
                CalFatLabel.Text = results.ToString();
            }
            else
            {
                MessageBox.Show("Enter an Integer value.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void CarbsConvBtn_Click(object sender, EventArgs e)
        {
             if (double.TryParse(CarbsGramsTextBox.Text, out double carbGrams))
            {
                double results = CarbCalories(carbGrams);
                CalCarbLabel.Text = results.ToString();
            }
            else
            {
                MessageBox.Show("Enter an Integer value.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private double FatCalories(double fatGrams)
        {
            return fatGrams * 9;
        }
        private double CarbCalories(double carbGrams)
        {
            return carbGrams * 4;
        }

        private void ClrFatBtn_Click(object sender, EventArgs e)
        {
            FatGramsTextBox.Text = "";
            CalFatLabel.Text = "";
        }

        private void ClrCarbsBtn_Click(object sender, EventArgs e)
        {
            CarbsGramsTextBox.Text = "";
            CalCarbLabel.Text = "";
        }

        private void ExtBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
