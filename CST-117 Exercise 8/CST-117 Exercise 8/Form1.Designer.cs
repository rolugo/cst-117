﻿namespace CST_117_Exercise_8
{
    partial class NutritionistCal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FatGramsLabel = new System.Windows.Forms.Label();
            this.CarbGramsLabel = new System.Windows.Forms.Label();
            this.CalFromFatLabel = new System.Windows.Forms.Label();
            this.CalFromCarbsLabel = new System.Windows.Forms.Label();
            this.FatConvBtn = new System.Windows.Forms.Button();
            this.CarbsConvBtn = new System.Windows.Forms.Button();
            this.FatGramsTextBox = new System.Windows.Forms.TextBox();
            this.CalFatLabel = new System.Windows.Forms.TextBox();
            this.CarbsGramsTextBox = new System.Windows.Forms.TextBox();
            this.CalCarbLabel = new System.Windows.Forms.TextBox();
            this.ClrFatBtn = new System.Windows.Forms.Button();
            this.ClrCarbsBtn = new System.Windows.Forms.Button();
            this.ExtBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // FatGramsLabel
            // 
            this.FatGramsLabel.AutoSize = true;
            this.FatGramsLabel.Location = new System.Drawing.Point(69, 30);
            this.FatGramsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FatGramsLabel.Name = "FatGramsLabel";
            this.FatGramsLabel.Size = new System.Drawing.Size(58, 13);
            this.FatGramsLabel.TabIndex = 1;
            this.FatGramsLabel.Text = "Fat Grams:";
            // 
            // CarbGramsLabel
            // 
            this.CarbGramsLabel.AutoSize = true;
            this.CarbGramsLabel.Location = new System.Drawing.Point(62, 95);
            this.CarbGramsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CarbGramsLabel.Name = "CarbGramsLabel";
            this.CarbGramsLabel.Size = new System.Drawing.Size(65, 13);
            this.CarbGramsLabel.TabIndex = 3;
            this.CarbGramsLabel.Text = "Carb Grams:";
            // 
            // CalFromFatLabel
            // 
            this.CalFromFatLabel.AutoSize = true;
            this.CalFromFatLabel.Location = new System.Drawing.Point(42, 61);
            this.CalFromFatLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CalFromFatLabel.Name = "CalFromFatLabel";
            this.CalFromFatLabel.Size = new System.Drawing.Size(85, 13);
            this.CalFromFatLabel.TabIndex = 4;
            this.CalFromFatLabel.Text = "Calories from fat:";
            // 
            // CalFromCarbsLabel
            // 
            this.CalFromCarbsLabel.AutoSize = true;
            this.CalFromCarbsLabel.Location = new System.Drawing.Point(28, 127);
            this.CalFromCarbsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CalFromCarbsLabel.Name = "CalFromCarbsLabel";
            this.CalFromCarbsLabel.Size = new System.Drawing.Size(99, 13);
            this.CalFromCarbsLabel.TabIndex = 6;
            this.CalFromCarbsLabel.Text = "Calories from carbs:";
            // 
            // FatConvBtn
            // 
            this.FatConvBtn.Location = new System.Drawing.Point(224, 27);
            this.FatConvBtn.Margin = new System.Windows.Forms.Padding(2);
            this.FatConvBtn.Name = "FatConvBtn";
            this.FatConvBtn.Size = new System.Drawing.Size(66, 32);
            this.FatConvBtn.TabIndex = 8;
            this.FatConvBtn.Text = "Convert";
            this.FatConvBtn.UseVisualStyleBackColor = true;
            this.FatConvBtn.Click += new System.EventHandler(this.FatConvBtn_Click);
            // 
            // CarbsConvBtn
            // 
            this.CarbsConvBtn.Location = new System.Drawing.Point(224, 92);
            this.CarbsConvBtn.Margin = new System.Windows.Forms.Padding(2);
            this.CarbsConvBtn.Name = "CarbsConvBtn";
            this.CarbsConvBtn.Size = new System.Drawing.Size(66, 32);
            this.CarbsConvBtn.TabIndex = 9;
            this.CarbsConvBtn.Text = "Convert";
            this.CarbsConvBtn.UseVisualStyleBackColor = true;
            this.CarbsConvBtn.Click += new System.EventHandler(this.CarbsConvBtn_Click);
            // 
            // FatGramsTextBox
            // 
            this.FatGramsTextBox.Location = new System.Drawing.Point(131, 27);
            this.FatGramsTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.FatGramsTextBox.Name = "FatGramsTextBox";
            this.FatGramsTextBox.Size = new System.Drawing.Size(89, 20);
            this.FatGramsTextBox.TabIndex = 10;
            // 
            // CalFatLabel
            // 
            this.CalFatLabel.Location = new System.Drawing.Point(131, 58);
            this.CalFatLabel.Margin = new System.Windows.Forms.Padding(2);
            this.CalFatLabel.Name = "CalFatLabel";
            this.CalFatLabel.ReadOnly = true;
            this.CalFatLabel.Size = new System.Drawing.Size(89, 20);
            this.CalFatLabel.TabIndex = 11;
            // 
            // CarbsGramsTextBox
            // 
            this.CarbsGramsTextBox.Location = new System.Drawing.Point(131, 92);
            this.CarbsGramsTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.CarbsGramsTextBox.Name = "CarbsGramsTextBox";
            this.CarbsGramsTextBox.Size = new System.Drawing.Size(89, 20);
            this.CarbsGramsTextBox.TabIndex = 12;
            // 
            // CalCarbLabel
            // 
            this.CalCarbLabel.Location = new System.Drawing.Point(131, 124);
            this.CalCarbLabel.Margin = new System.Windows.Forms.Padding(2);
            this.CalCarbLabel.Name = "CalCarbLabel";
            this.CalCarbLabel.ReadOnly = true;
            this.CalCarbLabel.Size = new System.Drawing.Size(89, 20);
            this.CalCarbLabel.TabIndex = 13;
            // 
            // ClrFatBtn
            // 
            this.ClrFatBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClrFatBtn.Location = new System.Drawing.Point(297, 27);
            this.ClrFatBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ClrFatBtn.Name = "ClrFatBtn";
            this.ClrFatBtn.Size = new System.Drawing.Size(99, 32);
            this.ClrFatBtn.TabIndex = 14;
            this.ClrFatBtn.Text = "Clear Fat";
            this.ClrFatBtn.UseVisualStyleBackColor = true;
            this.ClrFatBtn.Click += new System.EventHandler(this.ClrFatBtn_Click);
            // 
            // ClrCarbsBtn
            // 
            this.ClrCarbsBtn.Location = new System.Drawing.Point(297, 92);
            this.ClrCarbsBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ClrCarbsBtn.Name = "ClrCarbsBtn";
            this.ClrCarbsBtn.Size = new System.Drawing.Size(99, 32);
            this.ClrCarbsBtn.TabIndex = 15;
            this.ClrCarbsBtn.Text = "Clear Carbs";
            this.ClrCarbsBtn.UseVisualStyleBackColor = true;
            this.ClrCarbsBtn.Click += new System.EventHandler(this.ClrCarbsBtn_Click);
            // 
            // ExtBtn
            // 
            this.ExtBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtBtn.Location = new System.Drawing.Point(439, 95);
            this.ExtBtn.Name = "ExtBtn";
            this.ExtBtn.Size = new System.Drawing.Size(90, 29);
            this.ExtBtn.TabIndex = 16;
            this.ExtBtn.Text = "Exit";
            this.ExtBtn.UseVisualStyleBackColor = true;
            this.ExtBtn.Click += new System.EventHandler(this.ExtBtn_Click);
            // 
            // NutritionistCal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::CST_117_Exercise_8.Properties.Resources.Background_pic;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(622, 353);
            this.Controls.Add(this.ExtBtn);
            this.Controls.Add(this.ClrCarbsBtn);
            this.Controls.Add(this.ClrFatBtn);
            this.Controls.Add(this.CalCarbLabel);
            this.Controls.Add(this.CarbsGramsTextBox);
            this.Controls.Add(this.CalFatLabel);
            this.Controls.Add(this.FatGramsTextBox);
            this.Controls.Add(this.CarbsConvBtn);
            this.Controls.Add(this.FatConvBtn);
            this.Controls.Add(this.CalFromCarbsLabel);
            this.Controls.Add(this.CalFromFatLabel);
            this.Controls.Add(this.CarbGramsLabel);
            this.Controls.Add(this.FatGramsLabel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NutritionistCal";
            this.Text = "Nutritionist Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

#endregion
        private System.Windows.Forms.Label FatGramsLabel;
        private System.Windows.Forms.Label CarbGramsLabel;
        private System.Windows.Forms.Label CalFromFatLabel;
        private System.Windows.Forms.Label CalFromCarbsLabel;
        private System.Windows.Forms.Button FatConvBtn;
        private System.Windows.Forms.Button CarbsConvBtn;
        private System.Windows.Forms.TextBox FatGramsTextBox;
        private System.Windows.Forms.TextBox CalFatLabel;
        private System.Windows.Forms.TextBox CarbsGramsTextBox;
        private System.Windows.Forms.TextBox CalCarbLabel;
        private System.Windows.Forms.Button ClrFatBtn;
        private System.Windows.Forms.Button ClrCarbsBtn;
        private System.Windows.Forms.Button ExtBtn;
    }
}
