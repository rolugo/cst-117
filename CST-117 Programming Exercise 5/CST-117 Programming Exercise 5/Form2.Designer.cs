﻿namespace CST_117_Programming_Exercise_5_V2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()

        {
            this.LuckyNumLabel = new System.Windows.Forms.Label();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            this.LuckyNumlabel2 = new System.Windows.Forms.Label();
            this.Restartbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // LuckyNumLabel
            // 
            this.LuckyNumLabel.AutoSize = true;
            this.LuckyNumLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(242)))), ((int)(((byte)(0)))));
            this.LuckyNumLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LuckyNumLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LuckyNumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 38.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuckyNumLabel.ForeColor = System.Drawing.Color.Firebrick;
            this.LuckyNumLabel.Image = global::CST_117_Programming_Exercise_5_V2.Properties.Resources.star;
            this.LuckyNumLabel.Location = new System.Drawing.Point(258, 124);
            this.LuckyNumLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LuckyNumLabel.MaximumSize = new System.Drawing.Size(120, 90);
            this.LuckyNumLabel.MinimumSize = new System.Drawing.Size(120, 90);
            this.LuckyNumLabel.Name = "LuckyNumLabel";
            this.LuckyNumLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.LuckyNumLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LuckyNumLabel.Size = new System.Drawing.Size(120, 90);
            this.LuckyNumLabel.TabIndex = 1;
            this.LuckyNumLabel.Text = "1";
            this.LuckyNumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PictureBox
            // 
            this.PictureBox.Location = new System.Drawing.Point(21, 65);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(436, 317);
            this.PictureBox.TabIndex = 3;
            this.PictureBox.TabStop = false;
            // 
            // LuckyNumlabel2
            // 
            this.LuckyNumlabel2.AutoSize = true;
            this.LuckyNumlabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuckyNumlabel2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LuckyNumlabel2.Location = new System.Drawing.Point(12, 9);
            this.LuckyNumlabel2.Name = "LuckyNumlabel2";
            this.LuckyNumlabel2.Size = new System.Drawing.Size(436, 51);
            this.LuckyNumlabel2.TabIndex = 4;
            this.LuckyNumlabel2.Text = "Your lucky number is:";
            // 
            // Restartbtn
            // 
            this.Restartbtn.Location = new System.Drawing.Point(373, 359);
            this.Restartbtn.Name = "Restartbtn";
            this.Restartbtn.Size = new System.Drawing.Size(75, 23);
            this.Restartbtn.TabIndex = 5;
            this.Restartbtn.Text = "Restart";
            this.Restartbtn.UseVisualStyleBackColor = true;
            this.Restartbtn.Click += new System.EventHandler(this.Restartbtn_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(461, 394);
            this.Controls.Add(this.Restartbtn);
            this.Controls.Add(this.LuckyNumlabel2);
            this.Controls.Add(this.LuckyNumLabel);
            this.Controls.Add(this.PictureBox);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.Text = "LuckyNumberForm";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.Label LuckyNumlabel2;
        public System.Windows.Forms.Label LuckyNumLabel;
        private System.Windows.Forms.Button Restartbtn;
    }
}
