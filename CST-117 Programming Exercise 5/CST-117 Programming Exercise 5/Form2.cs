﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Programming_Exercise_5_V2
{

    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            PictureBox.ImageLocation = "https://media.giphy.com/media/3fIobf8erdUJO/giphy.gif";
        }

        private void Restartbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
