﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Programming_Exercise_5_V2
{
    public partial class Form1 : Form
    {
        int year, monthNumber, birthDay, daysInMonth, generatedNumber = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            for (int i = 0; (DateTime.Now.Year - i) >= 1950; i++)
            {
                BirtYrCmbBx.Items.Add(DateTime.Now.Year - i);
            }
        }

        private void BirthDayCmbBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            birthDay = BirthDayCmbBx.SelectedIndex + 1;
        }

        private void ClrBtn_Click(object sender, EventArgs e)
        {
            BirtYrCmbBx.Text = "";
            BirthMoCmbBx.Text = "";
            BirthDayCmbBx.Text = "";
            ColorTextBx.Text = "";
        }

        private void BirtYrCmbBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BirthDayCmbBx.Enabled == true)
            {
                BirthDayCmbBx.Items.Clear();
                monthNumber = MonthToNumber(BirthMoCmbBx.GetItemText(BirthMoCmbBx.SelectedIndex));
                year = int.Parse(BirtYrCmbBx.GetItemText(BirtYrCmbBx.SelectedIndex));

                daysInMonth = DateTime.DaysInMonth(year, monthNumber);

                for (int i = 1; i <= daysInMonth; i++)
                {
                    BirthDayCmbBx.Items.Add(i);
                }
            }
        }

        private void FindLuckyNumBtn_Click(object sender, EventArgs e)
        {
            if (BirtYrCmbBx.SelectedIndex != -1 &&
                BirthMoCmbBx.SelectedIndex != -1 &&
                BirthDayCmbBx.SelectedIndex != -1 &&
                ColorTextBx.Text != "")
            {
                char[] favoriteColorArray = ColorTextBx.Text.ToCharArray();
                int trigger = 0;

                generatedNumber = year / (monthNumber + birthDay);
                _ = favoriteColorArray.Length < 4 ? 4 : favoriteColorArray.Length;

                for (int i = 0; i < favoriteColorArray.Length; i++)
                {
                    if (trigger == 0)
                    {
                        generatedNumber += favoriteColorArray[i];
                        trigger++;
                    }
                    else if (trigger == 1)
                    {
                        generatedNumber = Math.Abs(generatedNumber - favoriteColorArray[i]);
                        trigger++;
                    }
                    else if (trigger == 2)
                    {
                        generatedNumber *= favoriteColorArray[i];
                        trigger++;
                    }
                    else if (trigger == 3)
                    {
                        generatedNumber /= favoriteColorArray[i];
                        trigger = 0;
                    }
                }

                Form2 Frm = new Form2();
                Frm.LuckyNumLabel.Text = generatedNumber.ToString();
                Frm.Show();
            }
            else
            {
                MessageBox.Show("Please enter the required information.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BirthMoCmbBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            monthNumber = MonthToNumber(BirthMoCmbBx.GetItemText(BirthMoCmbBx.SelectedIndex));
            BirthDayCmbBx.Items.Clear();
            BirthDayCmbBx.Enabled = true;

            year = int.Parse(BirtYrCmbBx.GetItemText(BirtYrCmbBx.SelectedIndex));
            daysInMonth = DateTime.DaysInMonth(year, monthNumber);

            for (int i = 1; i <= daysInMonth; i++)
            {
                BirthDayCmbBx.Items.Add(i);
            }
        }

        private int MonthToNumber(string monthName)
        {
            switch (monthName)
            {
                case "January":
                    return 1;
                case "February":
                    return 2;
                case "March":
                    return 3;
                case "April":
                    return 4;
                case "May":
                    return 5;
                case "June":
                    return 6;
                case "July":
                    return 7;
                case "August":
                    return 8;
                case "September":
                    return 9;
                case "October":
                    return 10;
                case "November":
                    return 11;
                default:
                    return 12;
            }
        }
    }
}
