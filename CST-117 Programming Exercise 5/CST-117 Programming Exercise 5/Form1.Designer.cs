﻿namespace CST_117_Programming_Exercise_5_V2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BirtYrCmbBx = new System.Windows.Forms.ComboBox();
            this.YrLabel = new System.Windows.Forms.Label();
            this.BirthMoLabel = new System.Windows.Forms.Label();
            this.BirthMoCmbBx = new System.Windows.Forms.ComboBox();
            this.BirthDayLabel = new System.Windows.Forms.Label();
            this.BirthDayCmbBx = new System.Windows.Forms.ComboBox();
            this.ColorLabel = new System.Windows.Forms.Label();
            this.ColorTextBx = new System.Windows.Forms.TextBox();
            this.FindLuckyNumBtn = new System.Windows.Forms.Button();
            this.ClrBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BirtYrCmbBx
            // 
            this.BirtYrCmbBx.FormattingEnabled = true;
            this.BirtYrCmbBx.Location = new System.Drawing.Point(158, 28);
            this.BirtYrCmbBx.Margin = new System.Windows.Forms.Padding(2);
            this.BirtYrCmbBx.Name = "BirtYrCmbBx";
            this.BirtYrCmbBx.Size = new System.Drawing.Size(125, 21);
            this.BirtYrCmbBx.TabIndex = 0;
            this.BirtYrCmbBx.SelectedIndexChanged += new System.EventHandler(this.BirtYrCmbBx_SelectedIndexChanged);
            // 
            // YrLabel
            // 
            this.YrLabel.AutoSize = true;
            this.YrLabel.Location = new System.Drawing.Point(23, 30);
            this.YrLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.YrLabel.Name = "YrLabel";
            this.YrLabel.Size = new System.Drawing.Size(101, 13);
            this.YrLabel.TabIndex = 1;
            this.YrLabel.Text = "Enter your birth year";
            // 
            // BirthMoLabel
            // 
            this.BirthMoLabel.AutoSize = true;
            this.BirthMoLabel.Location = new System.Drawing.Point(23, 67);
            this.BirthMoLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BirthMoLabel.Name = "BirthMoLabel";
            this.BirthMoLabel.Size = new System.Drawing.Size(110, 13);
            this.BirthMoLabel.TabIndex = 2;
            this.BirthMoLabel.Text = "Enter your birth month";
            // 
            // BirthMoCmbBx
            // 
            this.BirthMoCmbBx.FormattingEnabled = true;
            this.BirthMoCmbBx.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.BirthMoCmbBx.Location = new System.Drawing.Point(158, 65);
            this.BirthMoCmbBx.Margin = new System.Windows.Forms.Padding(2);
            this.BirthMoCmbBx.Name = "BirthMoCmbBx";
            this.BirthMoCmbBx.Size = new System.Drawing.Size(125, 21);
            this.BirthMoCmbBx.TabIndex = 3;
            this.BirthMoCmbBx.SelectedIndexChanged += new System.EventHandler(this.BirthMoCmbBx_SelectedIndexChanged);
            // 
            // BirthDayLabel
            // 
            this.BirthDayLabel.AutoSize = true;
            this.BirthDayLabel.Location = new System.Drawing.Point(26, 103);
            this.BirthDayLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BirthDayLabel.Name = "BirthDayLabel";
            this.BirthDayLabel.Size = new System.Drawing.Size(98, 13);
            this.BirthDayLabel.TabIndex = 4;
            this.BirthDayLabel.Text = "Enter your birth day";
            // 
            // BirthDayCmbBx
            // 
            this.BirthDayCmbBx.Enabled = false;
            this.BirthDayCmbBx.FormattingEnabled = true;
            this.BirthDayCmbBx.Location = new System.Drawing.Point(158, 101);
            this.BirthDayCmbBx.Margin = new System.Windows.Forms.Padding(2);
            this.BirthDayCmbBx.Name = "BirthDayCmbBx";
            this.BirthDayCmbBx.Size = new System.Drawing.Size(125, 21);
            this.BirthDayCmbBx.TabIndex = 5;
            this.BirthDayCmbBx.SelectedIndexChanged += new System.EventHandler(this.BirthDayCmbBx_SelectedIndexChanged);
            // 
            // ColorLabel
            // 
            this.ColorLabel.AutoSize = true;
            this.ColorLabel.Location = new System.Drawing.Point(26, 140);
            this.ColorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ColorLabel.Name = "ColorLabel";
            this.ColorLabel.Size = new System.Drawing.Size(119, 13);
            this.ColorLabel.TabIndex = 6;
            this.ColorLabel.Text = "Enter your favorite color";
            // 
            // ColorTextBx
            // 
            this.ColorTextBx.Location = new System.Drawing.Point(158, 137);
            this.ColorTextBx.Margin = new System.Windows.Forms.Padding(2);
            this.ColorTextBx.MaxLength = 15;
            this.ColorTextBx.Name = "ColorTextBx";
            this.ColorTextBx.Size = new System.Drawing.Size(125, 20);
            this.ColorTextBx.TabIndex = 7;
            // 
            // FindLuckyNumBtn
            // 
            this.FindLuckyNumBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindLuckyNumBtn.Location = new System.Drawing.Point(28, 187);
            this.FindLuckyNumBtn.Margin = new System.Windows.Forms.Padding(2);
            this.FindLuckyNumBtn.Name = "FindLuckyNumBtn";
            this.FindLuckyNumBtn.Size = new System.Drawing.Size(254, 49);
            this.FindLuckyNumBtn.TabIndex = 8;
            this.FindLuckyNumBtn.Text = "Find out your lucky number!";
            this.FindLuckyNumBtn.UseVisualStyleBackColor = true;
            this.FindLuckyNumBtn.Click += new System.EventHandler(this.FindLuckyNumBtn_Click);
            // 
            // ClrBtn
            // 
            this.ClrBtn.Location = new System.Drawing.Point(29, 242);
            this.ClrBtn.Name = "ClrBtn";
            this.ClrBtn.Size = new System.Drawing.Size(253, 23);
            this.ClrBtn.TabIndex = 9;
            this.ClrBtn.Text = "Clear all ";
            this.ClrBtn.UseVisualStyleBackColor = true;
            this.ClrBtn.Click += new System.EventHandler(this.ClrBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 265);
            this.Controls.Add(this.ClrBtn);
            this.Controls.Add(this.FindLuckyNumBtn);
            this.Controls.Add(this.ColorTextBx);
            this.Controls.Add(this.ColorLabel);
            this.Controls.Add(this.BirthDayCmbBx);
            this.Controls.Add(this.BirthDayLabel);
            this.Controls.Add(this.BirthMoCmbBx);
            this.Controls.Add(this.BirthMoLabel);
            this.Controls.Add(this.YrLabel);
            this.Controls.Add(this.BirtYrCmbBx);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Lucky Number";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox BirtYrCmbBx;
        private System.Windows.Forms.Label YrLabel;
        private System.Windows.Forms.Label BirthMoLabel;
        private System.Windows.Forms.ComboBox BirthMoCmbBx;
        private System.Windows.Forms.Label BirthDayLabel;
        private System.Windows.Forms.ComboBox BirthDayCmbBx;
        private System.Windows.Forms.Label ColorLabel;
        private System.Windows.Forms.TextBox ColorTextBx;
        private System.Windows.Forms.Button FindLuckyNumBtn;
        private System.Windows.Forms.Button ClrBtn;
    }
}
