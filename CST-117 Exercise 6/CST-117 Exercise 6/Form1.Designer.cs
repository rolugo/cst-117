﻿using System;

namespace CST_117_Exercise_6
{
    partial class RolldiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RolldiceForm));
            this.BtnRolldice = new System.Windows.Forms.Button();
            this.diceBox1 = new System.Windows.Forms.RichTextBox();
            this.diceBox2 = new System.Windows.Forms.RichTextBox();
            this.diceRoll1Label = new System.Windows.Forms.Label();
            this.diceRoll2Label = new System.Windows.Forms.Label();
            this.Exitbutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnRolldice
            // 
            this.BtnRolldice.BackColor = System.Drawing.Color.Transparent;
            this.BtnRolldice.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnRolldice.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRolldice.ForeColor = System.Drawing.Color.DarkRed;
            this.BtnRolldice.Location = new System.Drawing.Point(28, 217);
            this.BtnRolldice.Name = "BtnRolldice";
            this.BtnRolldice.Size = new System.Drawing.Size(207, 41);
            this.BtnRolldice.TabIndex = 0;
            this.BtnRolldice.Text = "Roll Dice!";
            this.BtnRolldice.UseVisualStyleBackColor = false;
            this.BtnRolldice.Click += new System.EventHandler(this.BtnRolldice_Click);
            // 
            // diceBox1
            // 
            this.diceBox1.BackColor = System.Drawing.Color.Black;
            this.diceBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.diceBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diceBox1.ForeColor = System.Drawing.Color.DarkRed;
            this.diceBox1.Location = new System.Drawing.Point(161, 37);
            this.diceBox1.Name = "diceBox1";
            this.diceBox1.Size = new System.Drawing.Size(120, 120);
            this.diceBox1.TabIndex = 3;
            this.diceBox1.Text = "";
            // 
            // diceBox2
            // 
            this.diceBox2.BackColor = System.Drawing.Color.Black;
            this.diceBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.diceBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diceBox2.ForeColor = System.Drawing.Color.DarkRed;
            this.diceBox2.Location = new System.Drawing.Point(292, 37);
            this.diceBox2.Name = "diceBox2";
            this.diceBox2.Size = new System.Drawing.Size(120, 120);
            this.diceBox2.TabIndex = 4;
            this.diceBox2.Text = "";
            // 
            // diceRoll1Label
            // 
            this.diceRoll1Label.AutoSize = true;
            this.diceRoll1Label.BackColor = System.Drawing.Color.Black;
            this.diceRoll1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diceRoll1Label.ForeColor = System.Drawing.Color.White;
            this.diceRoll1Label.Location = new System.Drawing.Point(199, 79);
            this.diceRoll1Label.Name = "diceRoll1Label";
            this.diceRoll1Label.Size = new System.Drawing.Size(0, 54);
            this.diceRoll1Label.TabIndex = 5;
            // 
            // diceRoll2Label
            // 
            this.diceRoll2Label.AutoSize = true;
            this.diceRoll2Label.BackColor = System.Drawing.Color.Black;
            this.diceRoll2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diceRoll2Label.ForeColor = System.Drawing.Color.White;
            this.diceRoll2Label.Location = new System.Drawing.Point(313, 79);
            this.diceRoll2Label.Name = "diceRoll2Label";
            this.diceRoll2Label.Size = new System.Drawing.Size(0, 54);
            this.diceRoll2Label.TabIndex = 6;
            // 
            // Exitbutton
            // 
            this.Exitbutton.BackColor = System.Drawing.Color.Transparent;
            this.Exitbutton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Exitbutton.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exitbutton.ForeColor = System.Drawing.Color.DarkRed;
            this.Exitbutton.Location = new System.Drawing.Point(292, 218);
            this.Exitbutton.Name = "Exitbutton";
            this.Exitbutton.Size = new System.Drawing.Size(140, 41);
            this.Exitbutton.TabIndex = 7;
            this.Exitbutton.Text = "Exit";
            this.Exitbutton.UseVisualStyleBackColor = false;
            this.Exitbutton.Click += new System.EventHandler(this.Exitbutton_Click);
            // 
            // RolldiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.BackgroundImage = global::CST_117_Exercise_6.Properties.Resources.Roll_Dice_Background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(458, 288);
            this.Controls.Add(this.Exitbutton);
            this.Controls.Add(this.diceRoll2Label);
            this.Controls.Add(this.diceRoll1Label);
            this.Controls.Add(this.diceBox2);
            this.Controls.Add(this.diceBox1);
            this.Controls.Add(this.BtnRolldice);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RolldiceForm";
            this.Text = "Dice Roller";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnRolldice;
        private System.Windows.Forms.RichTextBox diceBox1;
        private System.Windows.Forms.RichTextBox diceBox2;
        private System.Windows.Forms.Label diceRoll1Label;
        private System.Windows.Forms.Label diceRoll2Label;
        private System.Windows.Forms.Button Exitbutton;
    }
}

