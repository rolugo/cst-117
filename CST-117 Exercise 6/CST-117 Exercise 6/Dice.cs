﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Exercise_6
{
    class Dice
    {
        private int sideUp;
        private readonly int one = 1;
        private readonly int two = 2;
        private readonly int three = 3;
        private readonly int four = 4;
        private readonly int five = 5;
        private readonly int six = 6;
        int number = 0;


        public void Roll()
        {
            Random rand = new Random();
            number = rand.Next(6) + 1;
            if (number == 1)
            {
                sideUp = one;
            }
            else if (number == 2)
            {
                sideUp = two;
            }
            else if (number == 3)
            {
                sideUp = three;
            }
            else if (number == 4)
            {
                sideUp = four;
            }
            else if (number == 5)
            {
                sideUp = five;
            }
            else
            {
                sideUp = six;
            }

        }


       public int GetSideUp()
        {
            return sideUp;
        }
    }
}