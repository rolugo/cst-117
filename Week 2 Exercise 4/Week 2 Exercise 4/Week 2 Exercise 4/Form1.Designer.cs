﻿namespace Week_2_Exercise_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalculate = new System.Windows.Forms.Button();
            this.textBoxSec = new System.Windows.Forms.TextBox();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.labelSeconds = new System.Windows.Forms.Label();
            this.labelresult = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCalculate
            // 
            this.btnCalculate.BackColor = System.Drawing.Color.Navy;
            this.btnCalculate.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculate.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.btnCalculate.Location = new System.Drawing.Point(247, 283);
            this.btnCalculate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(164, 37);
            this.btnCalculate.TabIndex = 0;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = false;
            this.btnCalculate.Click += new System.EventHandler(this.BtnCalculate_Click);
            // 
            // textBoxSec
            // 
            this.textBoxSec.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSec.Location = new System.Drawing.Point(247, 164);
            this.textBoxSec.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxSec.Name = "textBoxSec";
            this.textBoxSec.Size = new System.Drawing.Size(164, 27);
            this.textBoxSec.TabIndex = 1;
            this.textBoxSec.TextChanged += new System.EventHandler(this.TextBoxSec_TextChanged);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxResult.Location = new System.Drawing.Point(247, 224);
            this.textBoxResult.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(482, 27);
            this.textBoxResult.TabIndex = 2;
            // 
            // labelSeconds
            // 
            this.labelSeconds.AutoSize = true;
            this.labelSeconds.BackColor = System.Drawing.Color.Navy;
            this.labelSeconds.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSeconds.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.labelSeconds.Location = new System.Drawing.Point(135, 167);
            this.labelSeconds.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelSeconds.Name = "labelSeconds";
            this.labelSeconds.Size = new System.Drawing.Size(104, 25);
            this.labelSeconds.TabIndex = 3;
            this.labelSeconds.Text = "Seconds:";
            // 
            // labelresult
            // 
            this.labelresult.AutoSize = true;
            this.labelresult.BackColor = System.Drawing.Color.Navy;
            this.labelresult.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelresult.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.labelresult.Location = new System.Drawing.Point(162, 227);
            this.labelresult.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.labelresult.Name = "labelresult";
            this.labelresult.Size = new System.Drawing.Size(76, 25);
            this.labelresult.TabIndex = 4;
            this.labelresult.Text = "Result:";
            this.labelresult.Click += new System.EventHandler(this.Labelresult_Click);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.BackColor = System.Drawing.Color.Navy;
            this.Title.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.Title.Location = new System.Drawing.Point(230, 56);
            this.Title.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(259, 38);
            this.Title.TabIndex = 5;
            this.Title.Text = "Time Calculator";
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Navy;
            this.btnExit.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.btnExit.Location = new System.Drawing.Point(286, 415);
            this.btnExit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(125, 37);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.UseWaitCursor = true;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.BackgroundImage = global::Week_2_Exercise_4.Properties.Resources.Background_image;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(744, 562);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.labelresult);
            this.Controls.Add(this.labelSeconds);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.textBoxSec);
            this.Controls.Add(this.btnCalculate);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.TextBox textBoxSec;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Label labelSeconds;
        private System.Windows.Forms.Label labelresult;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Button btnExit;
    }
}

