﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Week_2_Exercise_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Seconds_Click(object sender, EventArgs e)
        {

        }

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            int seconds;

            if (int.TryParse(textBoxSec.Text, out seconds))

            {

                //minutes
                if (seconds >= 60 && seconds < 3600)
                {
                    int minutes = seconds / 60;

                    if (minutes == 1)
                    {
                        textBoxResult.Text = seconds.ToString() + " seconds contains " + minutes.ToString() + " minute.";
                    }
                    else
                    {
                        textBoxResult.Text = seconds.ToString() + " seconds contains " + minutes.ToString() + " minutes.";
                    }
                }

                //hours
                else if (seconds >= 3600 && seconds < 86400)
                {
                    int hours = seconds / 3600;
                    if (hours == 1)
                    {
                        textBoxResult.Text = seconds.ToString() + " seconds contains " + hours.ToString() + " hour.";
                    }
                    else
                    {
                        textBoxResult.Text = seconds.ToString() + " seconds contains " + hours.ToString() + " hours.";
                    }
                }

                //days
                else if (seconds >= 86400)
                {
                    int days = seconds / 86400;
                    if (days == 1)
                    {
                        textBoxResult.Text = seconds.ToString() + " seconds contains " + days.ToString() + " day";
                    }
                    else
                    {
                        textBoxResult.Text = seconds.ToString() + " seconds contains " + days.ToString() + " days";
                    }
                }

                //less than 60 seconds
                else
                {
                    textBoxResult.Text = "Enter a whole number greater than 59.";
                }
            }
            else
            {
                MessageBox.Show("Enter a valid integer number.");
            }
        }

        private void TextBoxSec_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Labelresult_Click(object sender, EventArgs e)
        {

        }
    }
}
