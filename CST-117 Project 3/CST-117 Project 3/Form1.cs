﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Project_3
{
    public partial class StatsForm : Form
    {
        public StatsForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void OpenFileButton_Click(object sender, EventArgs e)
        {

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String Text;
                String[] Word;
                String firstWord = "";
                String lastWord = "";
                String longestWord = "";
                String mostVowels = "";
                char[] wordCharArray;
                int VowelCount;
                int mostVowelCount = 0;

                statsListBox.Items.Clear();

                try
                {
                    StreamReader textFile = new StreamReader(openFileDialog.FileName);

                    while (!textFile.EndOfStream)
                    {
                        Text = textFile.ReadLine();
                        Text = Text.ToLower();

                        Word = Text.Split(' ');

                        for (int i = 0; i < Word.Length; i++)
                        {
                            VowelCount = 0;
                            wordCharArray = Word[i].ToCharArray();

                            if (firstWord == "")
                            {
                                firstWord = Word[i];
                            }
                            else if (Word[i].CompareTo(firstWord) < 0)
                            {
                                firstWord = Word[i];
                            }
                            if (lastWord == "")
                            {
                                lastWord = Word[i];
                            }
                            else if (Word[i].CompareTo(lastWord) > 0)
                            {
                                lastWord = Word[i];
                            }

                            if (longestWord == "")
                            {
                                longestWord = Word[i];
                            }
                            else if (longestWord.Length < Word[i].Length)
                            {
                                longestWord = Word[i];
                            }

                            for (int j = 0; j < wordCharArray.Length; j++)
                            {
                                if
                               (wordCharArray[j] == 'a' ||
                                wordCharArray[j] == 'e' ||
                                wordCharArray[j] == 'i' ||
                                wordCharArray[j] == 'o' ||
                                wordCharArray[j] == 'u')
                                {
                                    VowelCount++;
                                }
                            }

                            if (VowelCount > mostVowelCount)
                            {
                                mostVowelCount = VowelCount;
                                mostVowels = Word[i];
                            }
                        }
                    }

                    statsListBox.Items.Add("First Word Alphabetically: " + firstWord);
                    statsListBox.Items.Add("Last Word Alphabetically: " + lastWord);
                    statsListBox.Items.Add("Longest Word: '" + longestWord + "' - " + longestWord.Length + " characters.");
                    statsListBox.Items.Add("Word with the most vowels: '" + mostVowels + "' - " + mostVowelCount + " vowels");
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)

        {

        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Title = "Open File";
            save.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamWriter write = new StreamWriter(File.Create(save.FileName));

                write.Write(Text);
                write.Dispose();
            }

            
        }
    }
}
